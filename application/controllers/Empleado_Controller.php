<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Empleado_Controller extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        $this->load->model('Empleado_Model');
        $this->load->library('upload');
        $this->load->model('Log_Model');
    }

    public function listado()
    {
        $list_empleados = $this->Empleado_Model->listar();
        $contenido = $this->load->view("Empleado_View/listado", array('list_empleados' => $list_empleados), TRUE);
        $this->load->view("Inicio_View/Inicio_Admin", array('contenido' => $contenido));
    }

    public function eliminar()
    {
        $idempleado = (int) $this->input->post('idemp');
        $dni_emp = (int) $this->input->post('dni__');
        $foto = $this->input->post('fotoo');
        if (!is_numeric($idempleado) || !isset($idempleado))
            die('recurso inexistente');
        $this->Empleado_Model->delete($idempleado, $foto);
        $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se elimino al usuario</div>');
        
        //logs
        $fecha=date("y-m-d");
        $hora=date("h:i:s");
        $usuario=$this->session->userdata("nombre_usu");
        $detalle="El usuario ".$usuario." Elimino al Empleado con dni: ".$dni_emp;
        $this->Log_Model->save($detalle,$fecha,$hora);

        redirect('Empleado_Controller/listado');
    }

    public function guardar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('dni', 'Dni', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('apellido', 'Apellido', 'required');
        $this->form_validation->set_rules('domicilio', 'Domicilio', 'required');
        $this->form_validation->set_rules('telefono', 'Telefono', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('horario', 'Horario', 'required');
        $this->form_validation->set_rules('sueldo', 'Sueldo', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo al Empleado porque no completo los datos obligatorios.</div>');
            redirect('Empleado_Controller/listado');
        } else {
            $dni_emp = trim($this->input->post('dni'));
            $nombre_emp = $this->input->post('nombre');
            $apellido_emp = $this->input->post('apellido');
            $domicilio_emp = $this->input->post('domicilio');
            $telefono_emp = $this->input->post('telefono');
            $email_emp = $this->input->post('email');
            $foto_emp = $this->input->post('foto');

            $config['upload_path'] = './assets/picture';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = '2048';  //2MB max
            $config['max_width'] = '4480'; // pixel
            $config['max_height'] = '4480'; // pixel
            $config['file_name'] = $_FILES['foto']['name'];
            $this->upload->initialize($config);
            $this->upload->do_upload('foto');
            $foto = $this->upload->data();

            $puesto_emp = $this->input->post('puesto');
            $horario_emp = $this->input->post('horario');
            $sueldo_emp = $this->input->post('sueldo');
            $resultado = $this->Empleado_Model->save($dni_emp, $nombre_emp, $apellido_emp, $domicilio_emp, $telefono_emp, $email_emp, $foto['file_name'], $puesto_emp, $horario_emp, $sueldo_emp);

            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se guardo correctamente</div>');
           
                //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Agrego un Empleado con dni: ".$dni_emp;
                $this->Log_Model->save($detalle,$fecha,$hora);

            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error no debe repetirse el id_emp.</div>');
            }
            redirect('Empleado_Controller/listado');
        }
    }

    public function actualizar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('dni_', 'Dni', 'required');
        $this->form_validation->set_rules('nombre_', 'Nombre', 'required');
        $this->form_validation->set_rules('apellido_', 'Apellido', 'required');
        $this->form_validation->set_rules('domicilio_', 'Domicilio', 'required');
        $this->form_validation->set_rules('telefono_', 'Telefono', 'required');
        $this->form_validation->set_rules('email_', 'Email', 'required');
        $this->form_validation->set_rules('puesto_', 'Puesto', 'required');
        $this->form_validation->set_rules('horario_', 'Horario', 'required');
        $this->form_validation->set_rules('sueldo_', 'Sueldo', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo el Empleado porque no completo los datos obligatorios.</div>');
            redirect('Empleado_Controller/listado');
        } else {
            $id_emp = $this->input->post('idempleado_');
            $dni_emp = $this->input->post('dni_');
            $nombre_emp = $this->input->post('nombre_');
            $apellido_emp = $this->input->post('apellido_');
            $domicilio_emp = $this->input->post('domicilio_');
            $telefono_emp = $this->input->post('telefono_');
            $email_emp = $this->input->post('email_');
            $foto_emp = $this->input->post('foto_');
            $foto_=$this->input->post('foto_old');
            $path = './assets/picture/';
            // get foto
            $config['upload_path'] = './assets/picture';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = '2048';  //2MB max
            $config['max_width'] = '4480'; // pixel
            $config['max_height'] = '4480'; // pixel
            $config['file_name'] = $_FILES['foto_']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['foto_']['name'])) {
                $this->upload->do_upload('foto_');
                $foto = $this->upload->data();
                @unlink($path . $this->input->post('foto_old'));
                $foto_=$foto['file_name'];
                // hapus foto pada direktori
                
            }

            $puesto_emp = $this->input->post('puesto_');
            $horario_emp = $this->input->post('horario_');
            $sueldo_emp = trim($this->input->post('sueldo_'));
            $id_emp = $this->input->post('idempleado_');
            $resultado = $this->Empleado_Model->update($id_emp, $dni_emp, $nombre_emp, $apellido_emp, $domicilio_emp, $telefono_emp, $email_emp, $foto_, $puesto_emp, $horario_emp, $sueldo_emp);
            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se actualizo correctamente</div>');
                //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Actualizo al Elmpleado con dni: ".$dni_emp;
                $this->Log_Model->save($detalle,$fecha,$hora);
            
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se actualizo.</div>');
            }
            redirect('Empleado_Controller/listado');
        }
    }
    
}
