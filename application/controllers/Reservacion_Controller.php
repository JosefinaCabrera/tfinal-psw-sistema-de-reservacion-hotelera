<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Reservacion_Controller extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        $this->load->model('Reservacion_Model');
        $this->load->model('Habitacion_Model');
        $this->load->model('Msg_Model');
        $this->load->model('Log_Model');
        $this->load->model('Cliente_Model');
    }

    public function listado()
    {
        $list_clientes = $this->Cliente_Model->listar();
        $list_habitaciones = $this->Habitacion_Model->listar();
        $list_reservaciones = $this->Reservacion_Model->listar();
        $ms=$this->Msg_Model->listarMsgNoLeidos();
        $list_transacciones = $this->Reservacion_Model->listarTransaccion();
        $contenido = $this->load->view("Reservacion_View/listado", array('list_reservaciones' => $list_reservaciones,'list_transacciones'=>$list_transacciones,'ms'=>$ms, 'list_habitaciones'=>$list_habitaciones, 'list_clientes'=>$list_clientes), true);

        if ($this->session->userdata('puesto_emp') == "Administrador") {
            $this->load->view("Inicio_View/Inicio_Admin", array('contenido' => $contenido));
        }
        if ($this->session->userdata('puesto_emp') == "Resepcion") {
            $this->load->view("Inicio_View/Inicio_Resep", array('contenido' => $contenido));
        }
    }

    public function eliminar()
    {
        $idreserva = (int) $this->input->post('idres');
        if (!is_numeric($idreserva) || !isset($idreserva))
            die('recurso inexistente');
        $this->Reservacion_Model->delete($idreserva);
        $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se elimino la reservacion</div>');
        redirect('Reservacion_Controller/listado');
    }

    public function guardar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_hab', 'IdHab', 'required');
        $this->form_validation->set_rules('fecha_entrada', 'FechaEntrada', 'required');
        $this->form_validation->set_rules('fecha_salida', 'FechaSalida', 'required');
        $this->form_validation->set_rules('estado_pagado', 'Estado', 'required');
        $this->form_validation->set_rules('id_cli', 'Idcli', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo la Reserva porque no completo los datos obligatorios.</div>');
            redirect('Reservacion_Controller/listado');
        } else {
            $id_hab_reser = trim($this->input->post('id_hab'));
            $fecha_entrada_reser = $this->input->post('fecha_entrada');
            $fecha_salida_reser = $this->input->post('fecha_salida');
            $detalle_reser = $this->input->post('detalle');
            $estado_pagado_reser = $this->input->post('estado_pagado');
            $id_cli_reser = $this->input->post('id_cli');
            $resultado = $this->Reservacion_Model->save($id_hab_reser, $fecha_entrada_reser, $fecha_salida_reser, $detalle_reser, $estado_pagado_reser, $id_cli_reser);

            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se guardo correctamente</div>');
                $text = "No Disponible";
                $estado = $this->Habitacion_Model->getIdEstado($text);
                $this->Habitacion_Model->update_estado_hab($id_hab_reser, $estado->id_estado_hab);
                //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Agrego la reservacion con id: ".$id_hab_reser;
                $this->Log_Model->save($detalle,$fecha,$hora);
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error No se Guardo</div>');
            }
            redirect('Reservacion_Controller/listado');
        }
    }

    public function actualizar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_hab_', 'IdHab', 'required');
        $this->form_validation->set_rules('fecha_entrada_', 'FechaEntrada', 'required');
        $this->form_validation->set_rules('fecha_salida_', 'FechaSalida', 'required');
        $this->form_validation->set_rules('estado_pagado_', 'Estado', 'required');
        $this->form_validation->set_rules('id_cli_', 'Idcli', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo la Reservacion porque no completo los datos obligatorios.</div>');
            redirect('Reservacion_Controller/listado');
        } else {
            $id_reser = $this->input->post('idreserva_');
            $id_hab_reser = $this->input->post('id_hab_');
            $fecha_entrada_reser = $this->input->post('fecha_entrada_');
            $fecha_salida_reser = $this->input->post('fecha_salida_');
            $detalle_reser = $this->input->post('detalle_');
            $estado_pagado_reser = $this->input->post('estado_pagado_');
            $id_cli_reser = $this->input->post('id_cli_');
            $resultado = $this->Reservacion_Model->update($id_reser, $id_hab_reser, $fecha_entrada_reser, $fecha_salida_reser, $detalle_reser, $estado_pagado_reser, $id_cli_reser);
            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se actualizo correctamente</div>');
                //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Actualizo la reservacion con id: ".$id_hab_reser;
                $this->Log_Model->save($detalle,$fecha,$hora);
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se actualizo.</div>');
            }
            redirect('Reservacion_Controller/listado');
        }
    }

    public function transaccion($idreserva = 0)
    {
        $reservacion = $this->Reservacion_Model->getReserva($idreserva);

        $date1 = date_create($reservacion->fecha_entrada_reser);
        $date2 = date_create($reservacion->fecha_salida_reser);
        $intervalo = date_diff($date1, $date2);
        $tiempo = array();
        foreach ($intervalo as $valor) {
            $tiempo[] = $valor;
        }

        $dias=$tiempo[11];
        $habitacion = $this->Habitacion_Model->getHab($reservacion->id_hab_reser);

        $totalprecio=$habitacion->precio_hab*$dias;

        $ms=$this->Msg_Model->listarMsgNoLeidos();
        $contenido = $this->load->view("Reservacion_View/transaccion", array('reservacion' => $reservacion, 'habitacion' => $habitacion, 'dias'=>$dias,'totalprecio'=>$totalprecio,'ms'=>$ms), TRUE);
        if ($this->session->userdata('puesto_emp') == "Administrador") {
            $this->load->view("Inicio_View/Inicio_Admin", array('contenido' => $contenido));
        }
        if ($this->session->userdata('puesto_emp') == "Resepcion") {
            $this->load->view("Inicio_View/Inicio_Resep", array('contenido' => $contenido));
        }
    }

    
    public function eliminarT()
    {
        $idtransaccion = (int) $this->input->post('idtra_');
        if (!is_numeric($idtransaccion) || !isset($idtransaccion))
            die('recurso inexistente');
        $this->Reservacion_Model->deleteTransaccion($idtransaccion);
        $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se elimino la transaccion</div>');
        redirect('Reservacion_Controller/listado');
    }

    public function guardarT()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_reser', 'IdReserva', 'required');
        $this->form_validation->set_rules('id_cli', 'IdCliente', 'required');
        $this->form_validation->set_rules('tipo_de_pago', 'TipoPago', 'required');
        $this->form_validation->set_rules('precio', 'Precio', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo la Transaccion porque no completo los datos obligatorios.</div>');
            redirect('Reservacion_Controller/listado');
        } else {
            $id_reser = trim($this->input->post('id_reser'));
            $id_cli = $this->input->post('id_cli');
            $tipo_de_pago = $this->input->post('tipo_de_pago');
            $descripcion = $this->input->post('descripcion');
            $precio = $this->input->post('precio');
            $resultado = $this->Reservacion_Model->saveTransaccion($id_reser, $id_cli, $tipo_de_pago, $descripcion, $precio);

            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se guardo correctamente</div>');
                
                //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Agrego la transaccion de la reserva con id: ".$id_reser;
                $this->Log_Model->save($detalle,$fecha,$hora);

            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error</div>');
            }
            redirect('Reservacion_Controller/listado');
        }
    }
}
