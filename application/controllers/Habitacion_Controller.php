<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Habitacion_Controller extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        $this->load->model('Habitacion_Model');
        $this->load->library('upload');
        $this->load->model('Msg_Model');
        $this->load->model('Log_Model');
    }

    public function listado()
    {
        $ms=$this->Msg_Model->listarMsgNoLeidos();
        $list_habitaciones = $this->Habitacion_Model->listar();
        $list_tipos_hab=$this->Habitacion_Model->listar_tipo();
        $list_estados_hab=$this->Habitacion_Model->listar_estado();
        $contenido = $this->load->view("Habitacion_View/listado", array('list_habitaciones'=>$list_habitaciones,'list_tipos_hab'=>$list_tipos_hab,'list_estados_hab'=>$list_estados_hab,'ms'=>$ms), true);
        
        if ($this->session->userdata('puesto_emp')=="Administrador") {
            $this->load->view("Inicio_View/Inicio_Admin", array('contenido' => $contenido));
        }
        if ($this->session->userdata('puesto_emp')=="Resepcion") {
            $this->load->view("Inicio_View/Inicio_Resep", array('contenido' => $contenido));
        }
    }

    public function listarEstado(){
        $list_estados=$this->Habitacion_Model->listar_estado();
        return $list_estados;
    }

    public function eliminar()
    {
        $idhabitacion = (int) $this->input->post('idhab');
        $nro_hab = (int) $this->input->post('nro');
        $foto = $this->input->post('fotoo');
        if (!is_numeric($idhabitacion) || !isset($idhabitacion))
            die('recurso inexistente');
        $this->Habitacion_Model->delete($idhabitacion, $foto);
        $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se elimino la habitacion</div>');
        //logs
        $fecha=date("y-m-d");
        $hora=date("h:i:s");
        $usuario=$this->session->userdata("nombre_usu");
        $detalle="El usuario ".$usuario." Elimino la habitacion N° ".$nro_hab;
        $this->Log_Model->save($detalle,$fecha,$hora);
        
        redirect('Habitacion_Controller/listado');

    }

    public function guardar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nro', 'Nro', 'required');
        $this->form_validation->set_rules('max_per', 'MaxPer', 'required');
        $this->form_validation->set_rules('precio', 'Precio', 'required');
        $this->form_validation->set_rules('tipoo', 'Tipo');
        $this->form_validation->set_rules('descripcion', 'Descripcion', 'required');
        $this->form_validation->set_rules('estadoo', 'Estado');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo la Habitacion porque no completo los datos obligatorios.</div>');
            redirect('Habitacion_Controller/listado');
        } else {
            $nro_hab = trim($this->input->post('nro'));
            $max_per_hab = $this->input->post('max_per');
            $precio_hab = $this->input->post('precio');
            $tipo_hab = $this->input->post('tipoo');
            $descripcion_hab = $this->input->post('descripcion');
            $estado_hab = $this->input->post('estadoo');
            $foto_hab = $this->input->post('foto');

            $config['upload_path'] = './assets/picture';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = '2048';  //2MB max
            $config['max_width'] = '4480'; // pixel
            $config['max_height'] = '4480'; // pixel
            $config['file_name'] = $_FILES['foto']['name'];
            $this->upload->initialize($config);
            $this->upload->do_upload('foto');
            $foto = $this->upload->data();
            $resultado = $this->Habitacion_Model->save($nro_hab, $max_per_hab, $precio_hab, $tipo_hab, $descripcion_hab, $estado_hab, $foto['file_name']);

            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se guardo correctamente</div>');
                //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Agrego la habitacion N° ".$nro_hab;
                $this->Log_Model->save($detalle,$fecha,$hora);
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error no debe repetirse el nro.</div>');
            }
            redirect('Habitacion_Controller/listado');
        }
    }

    public function actualizar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nro_', 'Nro', 'required');
        $this->form_validation->set_rules('max_per_', 'MaxPer', 'required');
        $this->form_validation->set_rules('precio_', 'Precio', 'required');
        $this->form_validation->set_rules('tipooo', 'Tipo', 'required');
        $this->form_validation->set_rules('descripcion_', 'Descripcion', 'required');
        $this->form_validation->set_rules('estadooo', 'Estado', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo la Habitacion porque no completo los datos obligatorios.</div>');
            redirect('Habitacion_Controller/listado');
        } else {
            $nro_hab = trim($this->input->post('nro_'));
            $max_per_hab = $this->input->post('max_per_');
            $precio_hab = $this->input->post('precio_');
            $tipo_hab = $this->input->post('tipooo');
            $descripcion_hab = $this->input->post('descripcion_');
            $estado_hab = $this->input->post('estadooo');
            $foto_hab = $this->input->post('foto_');
            $foto_=$this->input->post('foto_old');
            $path = './assets/picture/';
            // get foto
            $config['upload_path'] = './assets/picture';
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = '2048';  //2MB max
            $config['max_width'] = '4480'; // pixel
            $config['max_height'] = '4480'; // pixel
            $config['file_name'] = $_FILES['foto_']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['foto_']['name'])) {
                $this->upload->do_upload('foto_');
                $foto = $this->upload->data();
                @unlink($path . $this->input->post('foto_old'));
                $foto_=$foto['file_name'];
            }

            $id_hab = $this->input->post('idhabitacion_');
            $resultado = $this->Habitacion_Model->update($id_hab, $nro_hab, $max_per_hab, $precio_hab, $tipo_hab, $descripcion_hab, $estado_hab, $foto_);
            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se actualizo correctamente</div>');
            
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Actualizo la habitacion N° ".$nro_hab;
                $this->Log_Model->save($detalle,$fecha,$hora);
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se actualizo.</div>');
            }
            redirect('Habitacion_Controller/listado');
        }
    }

    public function eliminar_tipo()
    {
        $idtipo = (int) $this->input->post('idtipo');
        if (!is_numeric($idtipo) || !isset($idtipo))
            die('recurso inexistente');
        $this->Habitacion_Model->delete_tipo($idtipo);
        $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se elimino el tipo</div>');
        redirect('Habitacion_Controller/listado');

        
    }
    public function guardar_tipo()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('tipo_hab', 'Tipo', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo el Tipo porque no completo los datos obligatorios.</div>');
            redirect('Habitacion_Controller/listado');
        } else {
            $tipo_hab = trim($this->input->post('tipo_hab'));

            $resultado = $this->Habitacion_Model->save_tipo($tipo_hab);

            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se guardo correctamente</div>');
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error No se Guardo</div>');
            }
            redirect('Habitacion_Controller/listado');
        }
    }
    public function eliminar_estado()
    {
        $idestado = (int) $this->input->post('idestado');
        if (!is_numeric($idestado) || !isset($idestado))
            die('recurso inexistente');
        $this->Habitacion_Model->delete_estado($idestado);
        $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se elimino el Estado</div>');
        redirect('Habitacion_Controller/listado');
    }
    public function guardar_estado()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('estado_hab', 'estado', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo el Estado porque no completo los datos obligatorios.</div>');
            redirect('Habitacion_Controller/listado');
        } else {
            $estado_hab = trim($this->input->post('estado_hab'));

            $resultado = $this->Habitacion_Model->save_estado($estado_hab);

            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se guardo correctamente</div>');
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error No se Guardo</div>');
            }
            redirect('Habitacion_Controller/listado');
        }
    }

}