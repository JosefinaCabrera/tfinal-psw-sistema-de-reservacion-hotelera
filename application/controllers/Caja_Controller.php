<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Caja_Controller extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        $this->load->model('Caja_Model');
        $this->load->model('Cliente_Model');
        $this->load->model('Reservacion_Model');
        $this->load->model('Habitacion_Model');
        $this->load->model('Msg_Model');
        $this->load->model('Log_Model');
    }

    public function listado()
    {
        $ms=$this->Msg_Model->listarMsgNoLeidos();
        $ms=$this->Msg_Model->listarMsgNoLeidos();
        $list_cajas = $this->Caja_Model->listar();
        $contenido = $this->load->view("Caja_View/listado", array('list_cajas'=>$list_cajas,'ms'=>$ms), true);
        
        if ($this->session->userdata('puesto_emp')=="Administrador") {
            $this->load->view("Inicio_View/Inicio_Admin", array('contenido' => $contenido));
        }
        if ($this->session->userdata('puesto_emp')=="Resepcion") {
            $this->load->view("Inicio_View/Inicio_Resep", array('contenido' => $contenido));
        }
    }

    public function guardar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('cliente', 'Cliente', 'required');
        $this->form_validation->set_rules('total', 'Total', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo la Corte de Caja porque no completo los datos obligatorios.</div>');
            redirect('Caja_Controller/listado');
        } else {
            $cliente = trim($this->input->post('cliente'));
            $descripcion = $this->input->post('descripcion');
            $fecha = date("y-m-d");
            $total = $this->input->post('total');

            $idres = $this->input->post('idres_');

            $cliente = $this->Cliente_Model->getCli($cliente);

            $nom=$cliente->nombre_cli;
            $ape=$cliente->apellido_cli;
            $cliNomYApe=$nom.' '.$ape;

            $resultado = $this->Caja_Model->save($cliNomYApe, $descripcion, $fecha, $total);

            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se guardo correctamente</div>');
                $estadoP='si';
                $this->Reservacion_Model->updateEstadoPagado($idres, $estadoP);

                //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." cerro Caja del Cliente ".$cliNomYApe;
                $this->Log_Model->save($detalle,$fecha,$hora);

                $text = "Disponible";
                $estado = $this->Habitacion_Model->getIdEstado($text);
                $reservacion = $this->Reservacion_Model->getReserva($idres);
                $this->Habitacion_Model->update_estado_hab($reservacion->id_hab_reser, $estado->id_estado_hab);
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error</div>');
            }
            redirect('Caja_Controller/listado');
        }
    }


}
