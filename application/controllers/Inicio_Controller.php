<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Inicio_Controller extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
            $this->load->model('Msg_Model');
    }

    public function index()
    {
        /*
        if (!file_exists('application/views/login_view.php'))
            show_error('Lo sentimos la pagina no esta disponible en estos momentos - Comuniquese con el administrador de Sistema');
        die('-----');
        */
        $ms=$this->Msg_Model->listarMsgNoLeidos();
        $contenido = $this->load->view("Principal_View", array('ms'=>$ms), true);
        if($this->session->userdata('puesto_emp')=="Administrador"){
            $this->load->view("Inicio_View/Inicio_Admin.php", array('contenido' => $contenido));
        }
        if($this->session->userdata('puesto_emp')=="Resepcion"){
            $this->load->view("Inicio_View/Inicio_Resep.php", array('contenido' => $contenido));
        }
    }

}