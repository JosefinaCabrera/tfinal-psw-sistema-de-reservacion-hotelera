<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cliente_Controller extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        $this->load->model('Cliente_Model');
        $this->load->model('Msg_Model');
        $this->load->model('Log_Model');
    }

    public function listado()
    {
        $ms=$this->Msg_Model->listarMsgNoLeidos();
        $list_clientes = $this->Cliente_Model->listar();
        $contenido = $this->load->view("Cliente_View/listado", array('list_clientes'=>$list_clientes,'ms'=>$ms), true);
        
        if ($this->session->userdata('puesto_emp')=="Administrador") {
            $this->load->view("Inicio_View/Inicio_Admin", array('contenido' => $contenido));
        }
        if ($this->session->userdata('puesto_emp')=="Resepcion") {
            $this->load->view("Inicio_View/Inicio_Resep", array('contenido' => $contenido));
        }
    }

    public function eliminar()
    {
        $idcliente = (int) $this->input->post('idcli');
        $dni = (int) $this->input->post('dnicli');
        if (!is_numeric($idcliente) || !isset($idcliente))
            die('recurso inexistente');
        $this->Cliente_Model->delete($idcliente);
        $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se elimino al cliente</div>');
        //logs
        $fecha=date("y-m-d");
        $hora=date("h:i:s");
        $usuario=$this->session->userdata("nombre_usu");
        $detalle="El usuario ".$usuario." Elimino al Huesped con dni: ".$dni;
        $this->Log_Model->save($detalle,$fecha,$hora);
        
        redirect('Cliente_Controller/listado');
    }

    public function guardar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('dni', 'Dni', 'required');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('apellido', 'Apellido', 'required');
        $this->form_validation->set_rules('domicilio', 'Domicilio', 'required');
        $this->form_validation->set_rules('telefono', 'Telefono', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo el Cliente porque no completo los datos obligatorios.</div>');
            redirect('Cliente_Controller/listado');
        } else {
            $dni_cli = trim($this->input->post('dni'));
            $nombre_cli = $this->input->post('nombre');
            $apellido_cli = $this->input->post('apellido');
            $domicilio_cli = $this->input->post('domicilio');
            $telefono_cli = $this->input->post('telefono');
            $email_cli = $this->input->post('email');

            $resultado = $this->Cliente_Model->save($dni_cli, $nombre_cli, $apellido_cli, $domicilio_cli, $telefono_cli, $email_cli);

            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se guardo correctamente</div>');
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error, el DNI o Email ingresados ya existe</div>');
                //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Agrego al Huesped con dni: ".$dni_cli;
                $this->Log_Model->save($detalle,$fecha,$hora);
            }
            redirect('Cliente_Controller/listado');
        }
    }

    public function actualizar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('dni_', 'Dni', 'required');
        $this->form_validation->set_rules('nombre_', 'Nombre', 'required');
        $this->form_validation->set_rules('apellido_', 'Apellido', 'required');
        $this->form_validation->set_rules('domicilio_', 'Domicilio', 'required');
        $this->form_validation->set_rules('telefono_', 'Telefono', 'required');
        $this->form_validation->set_rules('email_', 'Email', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo al Cliente porque no completo los datos obligatorios.</div>');
            redirect('Cliente_Controller/listado');
        } else {
            $id_cli = $this->input->post('idcliente_');
            $dni_cli = $this->input->post('dni_');
            $nombre_cli = $this->input->post('nombre_');
            $apellido_cli = $this->input->post('apellido_');
            $domicilio_cli = $this->input->post('domicilio_');
            $telefono_cli = $this->input->post('telefono_');
            $email_cli = $this->input->post('email_');

            $resultado = $this->Cliente_Model->update($id_cli,$dni_cli, $nombre_cli, $apellido_cli, $domicilio_cli, $telefono_cli, $email_cli);
            if ($resultado) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se actualizo correctamente</div>');
                //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Actualizo al Huesped con dni: ".$dni_cli;
                $this->Log_Model->save($detalle,$fecha,$hora);

            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se actualizo.</div>');
            }
            redirect('Cliente_Controller/listado');
        }
    }
}