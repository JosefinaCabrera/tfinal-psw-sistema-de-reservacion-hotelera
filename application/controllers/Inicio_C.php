<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Inicio_C extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        $this->load->model('Habitacion_Model');
        $this->load->model('Msg_Model');
    }

    public function index()
    {
        $list_hab = $this->Habitacion_Model->listar();
        $this->load->view("Inicio.php",array('list_hab' => $list_hab));
    }
    public function guardarConsulta()
    {
        $this->load->library('form_validation');        
        $this->form_validation->set_rules('nya', 'NyA', 'required');
        $this->form_validation->set_rules('tel', 'Tel', 'required');
        $this->form_validation->set_rules('emaill', 'Email', 'required');
        $this->form_validation->set_rules('consulta', 'Consulta', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se Envio la Consulta porque no completo los datos obligatorios.</div>');
            redirect(base_url());
        } else {
            $nombre_completo = trim($this->input->post('nya'));
            $nro_telefono = $this->input->post('tel');
            $email = $this->input->post('emaill');
            $consulta = $this->input->post('consulta');
            $fecha = date("y-m-d");
            $leido = "No";
            $resul = $this->Msg_Model->save($nombre_completo, $nro_telefono, $email, $consulta,$fecha,$leido);

            if ($resul) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se Envio correctamente / En breve te responderemos</div>');
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error de envio</div>');
            }
            redirect(base_url());
        }
    }
}
