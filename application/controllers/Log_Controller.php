<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Log_Controller extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        $this->load->model('Log_Model');
    
    }

    public function listado()
    {
        $listlog=$this->Log_Model->listar();
        $contenido = $this->load->view("Log_View/lista", array('listlog'=>$listlog), TRUE);
        
        $this->load->view("Inicio_View/Inicio_Admin", array('contenido' => $contenido));
        
       
    }
}   