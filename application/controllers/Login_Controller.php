<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_Controller extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        $this->load->model('Login_Model');
        $this->load->model('Empleado_Model');
    }

    public function index()
    {
        if (!file_exists('application/views/Login_View.php'))
            show_error('Lo sentimos la pagina no esta disponible en estos momentos - Comuniquese con el administrador de Sistema');
        $this->load->view('Login_View');
    }

    public function validar()
    {
        $data = array();
        $this->form_validation->set_rules('user', 'Usuario', 'required|trim');
        $this->form_validation->set_rules('password', 'Contraseña', 'required|trim');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No ingreso los campos obligatorios</div>');
            redirect('Login_Controller/index', 'refresh');
        } else {
            $usuario = $this->Login_Model->verificar_login(trim($this->input->post('user')), trim($this->input->post('password')));
            if ($usuario) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Inicio sesion correctamente</div>');
                $empleado = $this->Empleado_Model->getEmp($usuario->id_emp);
                $data = array('id_usu' => $usuario->id_usu, 'nombre_usu' => $usuario->nombre_usu, 'logged_in' => TRUE, 'foto_emp'=>$empleado->foto_emp, 'nombre_emp'=>$empleado->nombre_emp, 'puesto_emp'=>$empleado->puesto_emp);
                $this->session->set_userdata($data);
                redirect('Inicio_Controller', 'refresh');
            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Usuario/Password incorrectos</div>');
                redirect('Login_Controller', 'refresh');
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
