<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Usuario_Controller extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in'))
            redirect(base_url());
        $this->load->model('Usuario_Model');
        $this->load->model('Empleado_Model');
        $this->load->model('Log_Model');
        $this->load->model('Empleado_Model');
    }

    public function listado()
    {
        $list_empleados = $this->Empleado_Model->listar();
        $list_usuarios = $this->Usuario_Model->listar();
        $contenido = $this->load->view("Usuario_View/listado", array('list_usuarios' => $list_usuarios,'list_empleados'=>$list_empleados), TRUE);
        $this->load->view("Inicio_View/Inicio_Admin", array('contenido' => $contenido));
    }


    public function eliminar()
    {
        $idusuario = (int) $this->input->post('idusu');
        $nombre_usu = (int) $this->input->post('nom');
        if (!is_numeric($idusuario) || !isset($idusuario))
            die('recurso inexistente');
        $this->Usuario_Model->delete($idusuario);
        $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se elimino al usuario</div>');
        
        //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Elimino al Usuario: ".$nombre_usu;
                $this->Log_Model->save($detalle,$fecha,$hora);
        redirect('Usuario_Controller/listado');
    }

    public function guardar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('usuario', 'Usuario', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('idemp', 'IDEmp', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo el Usuario porque no completo los datos obligatorios.</div>');
            redirect('Usuario_Controller/listado');
        } else {
            $nombre_usu = trim($this->input->post('usuario'));
            $password_usu = $this->input->post('password');
            $id_emp = $this->input->post('idemp');
            $idusuario = $this->Usuario_Model->save($nombre_usu, $password_usu, $id_emp);

            if ($idusuario) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se guardo correctamente</div>');
            
                //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Agrego al Ususario con id: ".$nombre_usu;
                $this->Log_Model->save($detalle,$fecha,$hora);

            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error no debe repetirse el id_emp.</div>');
            }
            redirect('Usuario_Controller/listado');
        }
    }

    public function actualizar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('usuario_', 'Usuario', 'required');
        $this->form_validation->set_rules('password_', 'Password', 'required');
        $this->form_validation->set_rules('idemp_', 'IDEmp', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se guardo el Usuario porque no completo los datos obligatorios.</div>');
            redirect('Usuario_Controller/listado');
        } else {
            $nombre_usu = trim($this->input->post('usuario_'));
            $password_usu = $this->input->post('password_');
            $id_emp = $this->input->post('idemp_');
            $id_usu = $this->input->post('idusuario_');
            $exito = $this->Usuario_Model->update($id_usu, $nombre_usu, $password_usu, $id_emp);
            if ($exito) {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-success">Se actualizo correctamente</div>');
                
                //logs
                $fecha=date("y-m-d");
                $hora=date("h:i:s");
                $usuario=$this->session->userdata("nombre_usu");
                $detalle="El usuario ".$usuario." Actualizo el Usuario con dni: ".$nombre_usu;
                $this->Log_Model->save($detalle,$fecha,$hora);

            } else {
                $this->session->set_flashdata('mensaje', '<div class="alert alert-danger">No se actualizo.</div>');
            }
            redirect('Usuario_Controller/listado');
        }
    }
    public function verDetalle($idempleado=0)
    {
     $empleado = $this->Empleado_Model->getEmp($idempleado);
     $contenido = $this->load->view("Usuario_View/detalle", array('empleado'=>$empleado), TRUE);
     $this->load->view("Inicio_View/Inicio_Admin", array('contenido' => $contenido));    
    }

}
