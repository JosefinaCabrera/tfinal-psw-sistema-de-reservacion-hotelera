<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Impresion_Caja extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Caja_Model');
    }

    /** FUNCIONES PARA IMPRIMIR *///
    public function imprimirIdCaja($idcaja=0) {
        ob_clean();
        $this->load->helper('pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetFont('helvetica', '', 10);
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Hotel Frontera' . '', 'La Quiaca, Jujuy Argentina');
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();
        // Header      
        $caja = $this->Caja_Model->getCaja($idcaja);
        
        $txt = $this->load->view("Caja_View/detalle_impresion", array('caja' => $caja), true);
        $pdf->writeHTML($txt);
        //generar un codigo de barra
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        //$valor=sha1($empleado->id_empleado);        
        $pdf->write1DBarcode('1234567890128', 'EAN13', '', '', '', 18, 0.4, $style, 'N');
        $pdf->Output('detalle_caja_' . '.pdf', 'I');

        
    }

}
