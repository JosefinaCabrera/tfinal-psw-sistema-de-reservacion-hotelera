<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Msg_Controller extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        $this->load->model('Msg_Model');
    }

    public function listado()
    {
        $ms = $this->Msg_Model->listarMsgNoLeidos();
        $list_msg = $this->Msg_Model->listar();
        $contenido = $this->load->view("Msg_View/listado", array('list_msg' => $list_msg, 'ms' => $ms), TRUE);
        $this->load->view("Inicio_View/Inicio_Resep", array('contenido' => $contenido));
    }


    public function verMsg($idmsg = 0)
    {
        $ms = $this->Msg_Model->listarMsgNoLeidos();
        $msg = $this->Msg_Model->getMsg($idmsg);
        $contenido = $this->load->view("Msg_View/detalle", array('msg' => $msg, 'ms' => $ms), TRUE);
        $this->load->view("Inicio_View/Inicio_Resep", array('contenido' => $contenido));

        $this->Msg_Model->update_leido($idmsg, "Si");
    }
}
