<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Caja_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function listar()
    {
        $resultado = array();
        $sql = "SELECT * FROM corte_caja";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }


    public function save($cliente, $descripcion, $fecha, $total)
    {
        $this->db->insert('corte_caja', array('cliente' => $cliente, 'descripcion'=>$descripcion, 'fecha' => $fecha, 'total' => $total));
        return $this->db->insert_id();
    }


    public function getCaja($idcaja = 0)
    {
        if ($idcaja == 0 || !isset($idcaja))
            return false;
        $objeto = $this->db->query("SELECT * FROM corte_caja WHERE id_caja=" . (int) $idcaja);
        if ($objeto)
            return $objeto->row();
        return false;
    }
}
