<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Msg_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function listar()
    {
        $resultado = array();
        $sql = "SELECT * FROM mensages";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }

    public function listarMsgNoLeidos()
    {
        $resultado=0;
        $sql = "SELECT * FROM mensages WHERE leido = 'No' ";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->num_rows();
        return $resultado;

    }

    public function save($nombre_completo, $nro_telefono, $email, $consulta,$fecha,$leido)
    {
        $this->db->insert('mensages', array('nombre_completo' => $nombre_completo, 'nro_telefono'=>$nro_telefono, 'email' => $email, 'consulta' => $consulta,'fecha'=>$fecha,'leido'=>$leido));
        return $this->db->insert_id();
    }

    public function update_leido($id_msg, $leido)
    {
        $this->db->update('Mensages', array('leido' => $leido), array('id_msg' => $id_msg));
        return true;
    }

    public function getMsg($idmsg = 0)
    {
        if ($idmsg == 0 || !isset($idmsg))
            return false;
        $objeto = $this->db->query("SELECT * FROM mensages WHERE id_msg=" . (int) $idmsg);
        if ($objeto)
            return $objeto->row();
        return false;
    }

}