<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Log_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $resultado = array();
        $sql = "SELECT * FROM logs";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }

    public function save($log_detalle, $fecha, $hora)
    {
        $this->db->insert('logs', array('log_detalle' => $log_detalle, 'fecha' => $fecha, 'hora' => $hora));
        return $this->db->insert_id();
    }
}
