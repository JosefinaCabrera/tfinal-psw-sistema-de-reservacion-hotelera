<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Usuario_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $resultado = array();
        $sql = "SELECT * FROM usuario";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }

    public function delete($idusuario = 0)
    {
        $this->db->delete('usuario', array('id_usu' => $idusuario));
        return true;
    }

    public function save($nombre_usu, $password_usu, $id_emp)
    {
        $sql = "SELECT * FROM usuario WHERE id_emp = $id_emp ";
        $objeto = $this->db->query($sql);
        if ($objeto->row() == 0) {
            $this->db->insert('usuario', array('nombre_usu' => $nombre_usu, 'password_usu' => md5($password_usu), 'id_emp' => $id_emp));

            return $this->db->insert_id();
        } else {
            /*$this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error no debe repetirse el id_emp.</div>');*/
        }
    }

    public function update($id_usu, $nombre_usu, $password_usu, $id_emp)
    {       
            $this->db->update('usuario', array('nombre_usu' => $nombre_usu, 'password_usu' => md5($password_usu), 'id_emp' => $id_emp), array('id_usu' => $id_usu));
            return true;
    }
}
