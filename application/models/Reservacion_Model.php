<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Reservacion_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $resultado = array();
        $sql = "SELECT * FROM reservacion";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }

    public function delete($idreserva = 0)
    {
        $this->db->delete('reservacion', array('id_reser' => $idreserva));
        return true;
    }

    public function save($id_hab_reser, $fecha_entrada_reser, $fecha_salida_reser, $detalle_reser, $estado_pagado_reser, $id_cli_reser)
    {
        $this->db->insert('reservacion', array('id_hab_reser' => $id_hab_reser, 'fecha_entrada_reser' => $fecha_entrada_reser, 'fecha_salida_reser' => $fecha_salida_reser,'detalle_reser'=>$detalle_reser,'estado_pagado_reser'=>$estado_pagado_reser,'id_cli_reser'=>$id_cli_reser));
        return $this->db->insert_id();
    }

    public function update($id_reser, $id_hab_reser, $fecha_entrada_reser, $fecha_salida_reser, $detalle_reser, $estado_pagado_reser, $id_cli_reser)
    {
        $this->db->update('reservacion', array('id_hab_reser' => $id_hab_reser, 'fecha_entrada_reser' => $fecha_entrada_reser, 'fecha_salida_reser' => $fecha_salida_reser,'detalle_reser'=>$detalle_reser,'estado_pagado_reser'=>$estado_pagado_reser,'id_cli_reser'=>$id_cli_reser), array('id_reser' => $id_reser));
        return true;
    }
    public function updateEstadoPagado($id_reser, $estado_pagado_reser)
    {
        $this->db->update('reservacion', array('estado_pagado_reser'=>$estado_pagado_reser), array('id_reser' => $id_reser));
        return true;
    }

    public function getReserva($idreserva=0){
        $objeto = $this->db->query("SELECT * FROM reservacion WHERE id_reser=" . (int) $idreserva);
        if ($objeto)
            return $objeto->row();
        return false;
    }





    public function listarTransaccion()
    {
        $resultado = array();
        $sql = "SELECT * FROM transaccion";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }

    public function deleteTransaccion($idtransaccion=0)
    {
        $this->db->delete('transaccion', array('id_tra' => $idtransaccion));
        return true;
    }

    public function saveTransaccion($id_reser, $id_cli, $tipo_de_pago, $descripcion, $precio)
    {
        $this->db->insert('transaccion', array('id_reser' => $id_reser, 'id_cli'=>$id_cli, 'tipo_de_pago' => $tipo_de_pago, 'descripcion' => $descripcion,'precio'=>$precio));
        return $this->db->insert_id();
    }
    
}
