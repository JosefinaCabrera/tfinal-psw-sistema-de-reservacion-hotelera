<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Empleado_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $resultado = array();
        $sql = "SELECT * FROM empleado";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }


    public function delete($idempleado = 0, $foto)
    {
        $path = './assets/picture/';
        @unlink($path . $foto);
        $this->db->delete('empleado', array('id_emp' => $idempleado));
        return true;
    }

    public function save($dni_emp, $nombre_emp, $apellido_emp, $domicilio_emp, $telefono_emp, $email_emp, $foto_emp, $puesto_emp, $horario_emp, $sueldo_emp)
    {
        $sql = "SELECT * FROM empleado WHERE dni_emp = $dni_emp ";
        $objeto = $this->db->query($sql);
        if ($objeto->row() == 0) {

            $this->db->insert('empleado', array('dni_emp' => $dni_emp, 'nombre_emp' => $nombre_emp, 'apellido_emp' => $apellido_emp,  'domicilio_emp' => $domicilio_emp,  'telefono_emp' => $telefono_emp,  'email_emp' => $email_emp,  'foto_emp' => $foto_emp,  'puesto_emp' => $puesto_emp,  'horario_emp' => $horario_emp,  'sueldo_emp' => $sueldo_emp));
            return $this->db->insert_id();
        } else {
            /*$this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error no debe repetirse el id_emp.</div>');*/
        }
    }

    public function update($id_emp, $dni_emp, $nombre_emp, $apellido_emp, $domicilio_emp, $telefono_emp, $email_emp, $foto_emp, $puesto_emp, $horario_emp, $sueldo_emp)
    {
        $this->db->update('empleado', array('dni_emp' => $dni_emp, 'nombre_emp' =>$nombre_emp, 'apellido_emp' =>$apellido_emp, 'domicilio_emp' =>$domicilio_emp, 'telefono_emp' =>$telefono_emp, 'email_emp' =>$email_emp, 'foto_emp' =>$foto_emp, 'puesto_emp' =>$puesto_emp, 'horario_emp' =>$horario_emp, 'sueldo_emp' =>$sueldo_emp), array('id_emp' => $id_emp));
        return true;
    }


    public function getEmp($idempleado = 0)
    {
        if ($idempleado == 0 || !isset($idempleado))
            return false;
        $objeto = $this->db->query("SELECT * FROM empleado WHERE id_emp=" . (int) $idempleado);
        if ($objeto)
            return $objeto->row();
        return false;
    }
}
