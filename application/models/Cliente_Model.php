<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cliente_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $resultado = array();
        $sql = "SELECT * FROM cliente";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }


    public function delete($idcliente = 0)
    {
        $this->db->delete('cliente', array('id_cli' => $idcliente));
        return true;
    }

    public function save($dni_cli, $nombre_cli, $apellido_cli, $domicilio_cli, $telefono_cli, $email_cli)
    {
        $sql = "SELECT * FROM cliente WHERE dni_cli = $dni_cli ";
        $objeto = $this->db->query($sql);
        if ($objeto->row() == 0) {
            $this->db->insert('cliente', array('dni_cli' => $dni_cli, 'nombre_cli' => $nombre_cli, 'apellido_cli' => $apellido_cli,  'domicilio_cli' => $domicilio_cli,  'telefono_cli' => $telefono_cli,  'email_cli' => $email_cli));
            return $this->db->insert_id();
        }
    }

    public function update($id_cli,$dni_cli, $nombre_cli, $apellido_cli, $domicilio_cli, $telefono_cli, $email_cli)
    {
        $this->db->update('cliente', array('dni_cli' => $dni_cli, 'nombre_cli' => $nombre_cli, 'apellido_cli' => $apellido_cli,  'domicilio_cli' => $domicilio_cli,  'telefono_cli' => $telefono_cli,  'email_cli' => $email_cli), array('id_cli' => $id_cli));
        return true;
    }


    public function getCli($idcliente = 0)
    {
        if ($idcliente == 0 || !isset($idcliente))
            return false;
        $objeto = $this->db->query("SELECT * FROM cliente WHERE id_cli=" . (int) $idcliente);
        if ($objeto)
            return $objeto->row();
        return false;
    }

}