<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Habitacion_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function listar()
    {
        $resultado = array();
        $sql = "SELECT h.id_hab,h.nro_hab,h.max_per_hab,h.precio_hab,th.tipo_hab,h.descripcion_hab,eh.estado_hab,h.foto_hab FROM habitacion h,tipo_habitacion th, estado_habitacion eh WHERE h.estado_hab=eh.id_estado_hab AND h.tipo_hab=th.id_tipo_hab";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }


    public function delete($idhabitacion = 0, $foto)
    {
        $path = './assets/picture/';
        @unlink($path . $foto);
        $this->db->delete('habitacion', array('id_hab' => $idhabitacion));
        return true;
    }

    public function save($nro_hab, $max_per_hab, $precio_hab, $tipo_hab, $descripcion_hab, $estado_hab, $foto_hab)
    {
        $sql = "SELECT * FROM habitacion WHERE nro_hab = $nro_hab ";
        $objeto = $this->db->query($sql);
        if ($objeto->row() == 0) {
            $this->db->insert('habitacion', array('nro_hab' => $nro_hab, 'max_per_hab' => $max_per_hab, 'precio_hab' => $precio_hab,  'tipo_hab' => $tipo_hab,  'descripcion_hab' => $descripcion_hab,  'estado_hab' => $estado_hab,  'foto_hab' => $foto_hab));
            return $this->db->insert_id();
        } else {
            /*$this->session->set_flashdata('mensaje', '<div class="alert alert-danger">Error no debe repetirse el id_emp.</div>');*/
        }
    }

    public function update($id_hab, $nro_hab, $max_per_hab, $precio_hab, $tipo_hab, $descripcion_hab, $estado_hab, $foto_hab)
    {
        $this->db->update('habitacion', array('nro_hab' => $nro_hab, 'max_per_hab' => $max_per_hab, 'precio_hab' => $precio_hab,  'tipo_hab' => $tipo_hab,  'descripcion_hab' => $descripcion_hab,  'estado_hab' => $estado_hab,  'foto_hab' => $foto_hab), array('id_hab' => $id_hab));
        return true;
    }
    public function update_estado_hab($id_hab, $estado_hab)
    {
        $this->db->update('habitacion', array('estado_hab' => $estado_hab), array('id_hab' => $id_hab));
        return true;
    }

    public function getHab($idhab=0){
        if ($idhab == 0 || !isset($idhab))
            return false;
        $objeto = $this->db->query("SELECT * FROM habitacion WHERE id_hab=" . (int) $idhab);
        if ($objeto)
            return $objeto->row();
        return false;
    }


    public function listar_tipo()
    {
        $resultado = array();
        $sql = "SELECT * FROM tipo_habitacion";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }
    public function delete_tipo($idtipo = 0)
    {
        $this->db->delete('tipo_habitacion', array('id_tipo_hab' => $idtipo));
        return true;
    }
    public function save_tipo($tipo_hab)
    {
        $this->db->insert('tipo_habitacion', array('tipo_hab' => $tipo_hab));
        return $this->db->insert_id();
    }



    public function listar_estado()
    {
        $resultado = array();
        $sql = "SELECT * FROM estado_habitacion";
        $objeto = $this->db->query($sql);
        if ($objeto)
            $resultado = $objeto->result();
        return $resultado;
    }
    public function delete_estado($idestado = 0)
    {
        $this->db->delete('estado_habitacion', array('id_estado_hab' => $idestado));
        return true;
    }
    public function save_estado($estado_hab)
    {
        $this->db->insert('estado_habitacion', array('estado_hab' => $estado_hab));
        return $this->db->insert_id();
    }
    public function getIdEstado($text)
    {

        if (!isset($text)) {
            return false;
        } else {
            if ($text == "Disponible"){
                $objeto = $this->db->query("SELECT * FROM estado_habitacion WHERE estado_hab='".$text."'");
            }
            if ($text == "No Disponible"){
                $objeto = $this->db->query("SELECT * FROM estado_habitacion WHERE estado_hab='".$text."'");
            }                
            if ($objeto)
                return $objeto->row();
            return false;
        }

    }
}
