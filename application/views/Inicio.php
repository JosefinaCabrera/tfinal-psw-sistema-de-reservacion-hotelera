<!DOCTYPE html>
<html lang="en">

<head>
	<title>Hotel Frontera</title>
	<!-- for-mobile-apps -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Resort Inn Responsive , Smartphone Compatible web template , Samsung, LG, Sony Ericsson, Motorola web design" />

	<!-- //for-mobile-apps -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/Inicio/css/bootstrap.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/Inicio/css/font-awesome.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/Inicio/css/style1.css">
	<style>
		#contact {
			background: url(background.png);
		}

		#servicio {
			background: url(services.jpg);
		}
	</style>
</head>

<body>
	<!-- header -->
	<div class="banner-top" style="background-color: grey;">
		<div class="contact-bnr-w3-agile">
			<ul>
				<li class="menu__item" ><a  href="<?= site_url('Login_Controller') ?>" style="color: white; text-decoration: underline;">Inicia Sesión</a></li>
			</ul>
		</div>
	</div>
	<div class="w3_navigation">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<h1><a class="navbar-brand" href="index.php"> HOTEL FRONTERA
							<p class="logo_w3l_agile_caption">Tu resort de ensueño</p>
						</a></h1>
				</div>
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--iris">
						<ul class="nav navbar-nav menu__list">
							<li class="menu__item"><a href="#rooms" class="menu__link scroll">Habitaciones</a></li>
						</ul>
					</nav>
				</div>
			</nav>
		</div>
	</div>


	<div id="home">
		<img src="hotelf.png" style="width: 100%;">
	</div>


	<!--sevices-->
	<div class="advantages" id="servicio">
		<div class="container">
			<div class="advantages-main">
				<h3 class="title-w3-agileits">Nuestros servicios
				</h3>
				<div class="advantage-bottom">
					<div class="col-md-6 advantage-grid left-w3ls wow bounceInLeft" data-wow-delay="0.3s">
						<div class="advantage-block ">
							<i class="fa fa-credit-card" aria-hidden="true"></i>
							<h4>Quédate primero, paga después! </h4>
							<p>Sin embargo, en ciertas circunstancias y el deber o la obligación se producen con frecuencia que los placeres tienen a sus funciones,
								.</p>
							<p><i class="fa fa-check" aria-hidden="true"></i>Habitación decorada, con aire acondicionado
							</p>
							<p><i class="fa fa-check" aria-hidden="true"></i>Balcón privado
							</p>

						</div>
					</div>
					<div class="col-md-6 advantage-grid right-w3ls wow zoomIn" data-wow-delay="0.3s">
						<div class="advantage-block">
							<i class="fa fa-clock-o" aria-hidden="true"></i>
							<h4>Restaurante las 24 horas
							</h4>
							<p>Sin embargo, en ciertas circunstancias y el deber o la obligación se producen con frecuencia que los placeres tienen a sus funciones,
								.</p>
							<p><i class="fa fa-check" aria-hidden="true"></i>24 horas de servicio a la habitación
							</p>
							<p><i class="fa fa-check" aria-hidden="true"></i>Servicio de conserjería las 24 horas
							</p>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
	</div>
	<!--//sevices-->


	<!-- rooms & rates -->
	<div class="plans-section" id="rooms">
		<div class="container">
			<h3 class="title-w3-agileits title-black-wthree">Habitaciones y tarifas
			</h3>
			<div class="priceing-table-main">
				<?php foreach ($list_hab as $hab) : ?>
					<div class="col-md-3 price-grid">
						<div class="price-block agile">
							<div class="price-gd-top">
								<img src="<?= base_url() ?>assets/picture/<?= $hab->foto_hab ?>" alt=" " class="img-responsive" />
								<h4>N°<?= $hab->nro_hab ?></h4>
							</div>
							<div class="price-gd-bottom">
								<div class="price-list">
									<h3><?= $hab->nro_hab ?></h3>
									<h3><span>$</span><?= $hab->precio_hab ?></h3>
								</div>
								<div class="price-selet">
									<a href="#"> Reservar ahora</a>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				
				
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>

	<!-- visitors -->
	<!-- contact -->
	<section class="contact-w3ls" id="contact">
		<div class="container">
			<div class="col-lg-6 col-md-6 col-sm-6 contact-w3-agile2" data-aos="flip-left">
				<div class="contact-agileits">
					<h4>Contáctenos</h4>
					<p class="contact-agile2">Realizar consulta rapida</p>
					<div><?= $this->session->flashdata('mensaje'); ?> </div>
					<?= form_open('Inicio_C/guardarConsulta', 'class="was-validated" id="frm_msg_new"') ?>
						<div class="control-group form-group">

							<label class="contact-p1">Nombre completo:</label>
							<input type="text" class="form-control" name="nya" id="nya" required>
							<p class="help-block"></p>

						</div>
						<div class="control-group form-group">

							<label class="contact-p1">Número de teléfono:</label>
							<input type="tel" class="form-control" name="tel" id="tel" required>
							<p class="help-block"></p>

						</div>
						<div class="control-group form-group">

							<label class="contact-p1">Dirección de correo electrónico:</label>
							<input type="email" class="form-control" name="emaill" id="emaill" required>
							<p class="help-block"></p>

						</div>
						<div class="control-group form-group">

							<label class="contact-p1">Consulata:</label>
							<input type="text" class="form-control" name="consulta" id="consulta" required>
							<p class="help-block"></p>

						</div>

						<input type="submit" value="Enviar" class="btn btn-primary">
						<?= form_close() ?>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 contact-w3-agile1" data-aos="flip-right">
				<h4>Conectate con nosotros
				</h4>
				<p class="contact-agile1"><strong>Teléfono :</strong>+94 (65)222-44-55</p>
				<p class="contact-agile1"><strong>Email :</strong> <a href="hotelfronteralq@gmail.com">hotelfronteralq@gmail.com</a></p>
				<p class="contact-agile1"><strong>Direccion :</strong> lima, surco</p>

				<div class="social-bnr-agileits footer-icons-agileinfo">
					<ul class="social-icons3">
						<li><a href="#" class="fa fa-facebook icon-border facebook"> </a></li>
						<li><a href="#" class="fa fa-twitter icon-border twitter"> </a></li>
						<li><a href="#" class="fa fa-google-plus icon-border googleplus"> </a></li>

					</ul>
				</div>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3074.7905052320443!2d-77.84987248482734!3d39.586871613613056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c9f6a80ccf0661%3A0x7210426c67abc40!2sVirginia+Welcome+Center%2FSafety+Rest+Area!5e0!3m2!1sen!2sin!4v1485760915662"></iframe>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<!-- /contact -->



	<div class="copy">
		<p>© 2021 <a href="index.php">HOTEL FRONTERA LA QUIACA</a> </p>
	</div>
	<!--/footer -->

	<script src="<?= base_url() ?>assets/Inicio/js/jquery-2.1.4.min.js" charset="utf-8"></script>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event) {
				event.preventDefault();
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>

	<script src="<?= base_url(); ?>assets/bootstrap/js/bootstrap-3.1.1.min.js"></script>
</body>

</html>