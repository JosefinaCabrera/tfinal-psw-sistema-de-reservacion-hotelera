<div class="container-fluid">
    <h1 class="text-center">Logs </h1>
    <div><?= $this->session->flashdata('mensaje'); ?> </div>
    <div class="row" id="log">
        <div class="col-12">
            <div class="table-responsive">
                <table id="tablax" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <th>ID</th>
                        <th>Detalle</th>
                        <th>Fecha</th>
                        <th>Hora</th>                      
                    </thead>
                    <tbody>
                        <?php foreach ($listlog as $log) : ?>
                            <tr>
                                <td><?= $log->id_log ?></td>
                                <td><?= $log->log_detalle ?></td>
                                <td><?= $log->fecha ?></td>
                                <td><?= $log->hora ?></td>
                                
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>