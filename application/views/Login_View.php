<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="<?= base_url() ?>assets/LoginView_estilo.css">
    <script src='<?= base_url() ?>assets/jquery/jquery-slim.min.js'></script>
    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap-grid.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap-reboot.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= base_url() ?>assets/fontawesome/css/all.css">
    <script type="text/javascript">
        $(document).ready(function() {
            $("#user").focus();
        });
    </script>

    <title>Login</title>
</head>

<body > 
    <div class="container">
        <div><?= $this->session->flashdata('mensaje'); ?> </div>
        <div class="row justify-content-center" style="margin-top: 50px; ">
            <div class="col-md-5">
                <div class="card p-3" style="border-radius: 5%;">

                    <div class="card-header text-center text-uppercase h4 font-weight-light">
                        Ingreso al Sistema
                        <br>
                        <h2>Hotel Frontera</h2>
                    </div>

                    <?php echo form_open('Login_Controller/validar'); ?>

                    <div class="card-body py-5">
                        <div class="form-label-group">
                            <input type="text" id="user" name="user" class="form-control" placeholder="Usuario" required autofocus><br>
                        </div>

                        <div class="form-label-group">
                            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required><br>
                        </div>
                        
                    </div>

                    <div class="card-footer">
                        <button class="btn btn-lg btn-primary btn-block" type="submit" style="cursor: pointer">Iniciar Sesion</button>
                    </div>

                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
    </div>

</body>

</html>