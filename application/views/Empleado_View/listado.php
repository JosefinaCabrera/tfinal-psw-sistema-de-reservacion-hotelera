<script type="text/javascript">
    function setEliminar(id, foto, dni) {
        $("#idemp").attr('value', id);
        $("#fotoo").attr('value', foto);
        $("#dni__").attr('value', dni);
    }

    function setGuardar() {
        $("document").ready(function() {
            $('#frm_empleado_new').submit(function() {
                $("#submit_guardar").attr('disabled', true);
            });
        });
    }

    function setEditar(id,dni,nombre,apellido,domicilio,telefono,email,foto,puesto,horario,sueldo) {
        $("#idempleado_").attr('value', id);
        $("#dni_").attr('value', dni);
        $("#nombre_").attr('value', nombre);
        $("#apellido_").attr('value', apellido);
        $("#domicilio_").attr('value', domicilio);
        $("#telefono_").attr('value', telefono);
        $("#email_").attr('value', email);
        $("#foto_old").attr('value', foto);
        $("#puesto_").attr('value', puesto);
        $("#horario_").attr('value', horario);
        $("#sueldo_").attr('value', sueldo);
        $("document").ready(function() {
            $('#frm_empleado_edit').submit(function() {
                $("#submit_actualizar").attr('disabled', true);
            });
        });
    }
</script>
<style>
    th,
    td {
        padding: 0.4rem !important;
    }

    #Empleado {
        border: 1px solid grey;
        border-radius: 10px;
    }
</style>
<div class="container-fluid">
    <h1 class="text-center">Empleados <a href="javascript:void()" onclick="setGuardar()" data-toggle="modal" data-target="#myModalGuardar"><i class="fas fa-users"></i></a></h1>
    <div><?= $this->session->flashdata('mensaje'); ?> </div>
    <div class="row" id="Empleado">
        <div class="col-12">
            <div class="table-responsive">
                <table id="tablax" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <th>ID</th>
                        <th>DNI</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Domicilio</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>Foto</th>
                        <th>Puesto</th>
                        <th>Horario</th>
                        <th>Sueldo</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php foreach ($list_empleados as $empleado) : ?>
                            <tr>
                                <td><?= $empleado->id_emp ?></td>
                                <td><?= $empleado->dni_emp ?></td>
                                <td><?= $empleado->nombre_emp ?></td>
                                <td><?= $empleado->apellido_emp ?></td>
                                <td><?= $empleado->domicilio_emp ?></td>
                                <td><?= $empleado->telefono_emp ?></td>
                                <td><?= $empleado->email_emp ?></td>
                                <td>
                                    <img width="100" src="<?= base_url() ?>assets/picture/<?= $empleado->foto_emp ?>">
                                </td>
                                <td><?= $empleado->puesto_emp ?></td>
                                <td><?= $empleado->horario_emp ?></td>
                                <td><?= $empleado->sueldo_emp ?></td>
                                <td class="center-align">
                                    <a href="javascript:void()" onclick="setEditar('<?= $empleado->id_emp ?>','<?= $empleado->dni_emp ?>','<?= $empleado->nombre_emp ?>','<?= $empleado->apellido_emp ?>','<?= $empleado->domicilio_emp ?>','<?= $empleado->telefono_emp ?>','<?= $empleado->email_emp ?>','<?= $empleado->foto_emp ?>','<?= $empleado->puesto_emp ?>','<?= $empleado->horario_emp ?>','<?= $empleado->sueldo_emp ?>')" data-toggle="modal" data-target="#myModalEditar"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:void()" onclick="setEliminar('<?= $empleado->id_emp ?>','<?= $empleado->foto_emp ?>','<?= $empleado->dni_emp ?>')" data-toggle="modal" data-target="#myModalEliminar"><i class="fas fa-trash-alt"></i></a>
                                    <a href="" title="Ver Detalle"><i class="fas fa-search-plus"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalEliminar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Empleado</h4>
                </div>
                <?= form_open('Empleado_Controller/eliminar', 'id="formulario_eliminar"') ?>
                <div class="modal-body">
                    <input type="hidden" value="" name="idemp" id="idemp" />
                    <input type="hidden" value="" name="fotoo" id="fotoo" />
                    <input type="hidden" value="" name="dni__" id="dni__" />
                    Esta seguro de Eliminar al Empleado?

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalGuardar">
        <div class="modal-dialog" style="width: 1000px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Empleado</h4>
                </div>
                <?= form_open_multipart('Empleado_Controller/guardar', 'class="was-validated" id="frm_empleado_new" enctype="multipart/form-data"') ?>
                <div class="modal-body">

                    <div class="container text-center">
                        <input type="hidden" value="" name="idempleado" id="idempleado" />
                        <div class="row form-group">
                            <div class="col"><input type="number" id="dni" name="dni" class="form-control" placeholder="Dni" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" /></div>
                            <div class="col"><input type="text" id="apellido" name="apellido" class="form-control" placeholder="Apellido" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="domicilio" name="domicilio" class="form-control" placeholder="Domicilio" /></div>
                            <div class="col"><input type="number" id="telefono" name="telefono" class="form-control" placeholder="Telefono" /></div>
                            <div class="col"><input type="text" id="email" name="email" class="form-control" placeholder="Email" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-1"><label for="foto">Foto:</label></div>
                            <div class="col-5"><input type="file" id="foto" name="foto" class="form-control-file" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-2"><label for="puesto">Puesto:</label></div>
                            <div class="col"><?= form_dropdown("puesto", array('Administrador' => 'Administrador', 'Recepcion' => 'Recepcion', 'Guardia' => 'Guardia', 'Limpieza' => 'Limpieza'), array(), array('id' => "puesto", 'class' => 'form-control')) ?></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="txt" id="horario" name="horario" class="form-control" placeholder="Horario" /></div>
                            <div class="col"><input type="number" id="sueldo" name="sueldo" class="form-control" placeholder="Sueldo" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalEditar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Usuario</h4>
                </div>
                <?= form_open_multipart('Empleado_Controller/actualizar', 'class="was-validated" id="frm_empleado_edit"') ?>
                <div class="modal-body">
                    <div class="container text-center">
                        <input type="hidden" value="" name="idempleado_" id="idempleado_" />
                        <div class="row form-group">
                            <div class="col"><input type="number" id="dni_" name="dni_" class="form-control" placeholder="Dni" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="nombre_" name="nombre_" class="form-control" placeholder="Nombre" /></div>
                            <div class="col"><input type="text" id="apellido_" name="apellido_" class="form-control" placeholder="Apellido" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="domicilio_" name="domicilio_" class="form-control" placeholder="Domicilio" /></div>
                            <div class="col"><input type="number" id="telefono_" name="telefono_" class="form-control" placeholder="Telefono" /></div>
                            <div class="col"><input type="text" id="email_" name="email_" class="form-control" placeholder="Email" /></div>
                        </div>
                        <input type="hidden" value="" name="foto_old" id="foto_old" />
                        <div class="row form-group">
                            <div class="col-1"><label for="foto_">Foto:</label></div>
                            <div class="col-5"><input type="file" id="foto_" name="foto_" class="form-control-file" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-2"><label for="puesto_">Puesto:</label></div>
                            <div class="col"><?= form_dropdown("puesto_", array('Administrador' => 'Administrador', 'Recepcion' => 'Recepcion', 'Guardia' => 'Guardia', 'Limpieza' => 'Limpieza'), array(), array('id' => "puesto_", 'name'=>"puesto_",'value'=>"", 'class' => 'form-control')) ?></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="txt" id="horario_" name="horario_" class="form-control" placeholder="Horario" /></div>
                            <div class="col"><input type="number" id="sueldo_" name="sueldo_" class="form-control" placeholder="Sueldo" /></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

</div>