
<style>
    th,
    td {
        padding: 0.4rem !important;
    }

    #Caja {
        border: 1px solid grey;
        border-radius: 10px;
    }
</style>
<div class="container-fluid">
    <h1 class="text-center">Corte De Caja</h1>
    <div><?= $this->session->flashdata('mensaje'); ?> </div>
    <div class="row" id="Caja">
        <div class="col-12">
            <div class="table-responsive">
                <table id="tablax" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <th>ID</th>
                        <th>Cliente</th>
                        <th>Descripcion</th>
                        <th>Fecha</th>
                        <th>Total</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php foreach ($list_cajas as $caja) : ?>
                            <tr>
                                <td><?= $caja->id_caja ?></td>
                                <td><?= $caja->cliente ?></td>
                                <td><?= $caja->descripcion ?></td>
                                <td><?= $caja->fecha ?></td>
                                <td><?= $caja->total ?></td>
                                <td class="center-align">
                                    <?= anchor('Impresion_Caja/imprimirIdCaja/'.$caja->id_caja, '<i class="fas fa-file-pdf"></i>')?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>