<div class="container">
    <h2>Ticket de Corte de Caja</h2>
    <div>
        <div class="row">
            <div class="col"><strong><label>Cliente:</label></strong></div>
            <div class="col"><?= $caja->cliente ?></div>
        </div>
        <div class="row">
            <div class="col"><strong><label>Fecha:</label></strong></div>
            <div class="col"><?= $caja->fecha ?></div>
            <div class="col"><strong><label>Descripcion:</label></strong><?= $caja->descripcion ?></div>
        </div>
        <div class="row">
            <div class="col"><strong><label>Total a Pagar:</label></strong></div>
            <div class="col"><?= $caja->total ?></div>
        </div>
        <div class="row">
            <img src="<?= base_url() ?>./assets/picture/frame.png" alt="">
        </div>
    </div>
    <div>