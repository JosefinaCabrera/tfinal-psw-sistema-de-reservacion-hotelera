<script type="text/javascript">
    function setEliminar(id, foto, nro_hab) {
        $("#idhab").attr('value', id);
        $("#fotoo").attr('value', foto);
        $("#nro").attr('value', nro_hab);
    }

    function setGuardar(estado) {
        $("document").ready(function() {
            $('#frm_habitacion_new').submit(function() {
                $("#submit_guardar").attr('disabled', true);
            });
        });
    }

    function setEditar(id, nro, max, precio, tipo, descripcion, estado, foto) {
        $("#idhabitacion_").attr('value', id);
        $("#nro_").attr('value', nro);
        $("#max_per_").attr('value', max);
        $("#precio_").attr('value', precio);
        $("#tipooo").attr('value', tipo);
        $("#descripcion_").attr('value', descripcion);
        $("#estadooo").attr('value', estado);
        $("#foto_old").attr('value', foto);
        $("document").ready(function() {
            $('#frm_habitacion_edit').submit(function() {
                $("#submit_actualizar").attr('disabled', true);
            });
        });
    }

    function setEliminarTipo(id) {
        $("#idtipo").attr('value', id);
    }

    function setGuardarTipo() {
        $("document").ready(function() {
            $('#frm_tipo_hab_new').submit(function() {
                $("#submit_guardar").attr('disabled', true);
            });
        });
    }

    function setEliminarEstado(id) {
        $("#idestado").attr('value', id);
    }

    function setGuardarEstado() {
        $("document").ready(function() {
            $('#frm_estado_hab_new').submit(function() {
                $("#submit_guardar").attr('disabled', true);
            });
        });
    }
</script>
<style>
    th,
    td {
        padding: 0.4rem !important;
    }

    #Habitacion,
    #Tipo,
    #Estado {
        border: 1px solid grey;
        border-radius: 10px;
    }
</style>
<div class="container-fluid">
    <h1 class="text-center">Habitaciones <a href="javascript:void()" onclick="setGuardar('Habitacion_Controller/listarEstado')" data-toggle="modal" data-target="#myModalGuardar"><i class="fas fa-bed"></i></a></h1>
    <div><?= $this->session->flashdata('mensaje'); ?> </div>
    <div class="row" id="Habitacion">
        <div class="col-12">
            <div class="table-responsive">
                <table id="tablax" class="table table-striped table-bordered" style="width:100%">
                    <thead>

                        <th>Nro</th>
                        <th>Max_Per</th>
                        <th>Precio</th>
                        <th>Tipo</th>
                        <th>Descripcion</th>
                        <th>Estado</th>
                        <th>Foto</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php foreach ($list_habitaciones as $habitacion) : ?>
                            <tr>

                                <td><?= $habitacion->nro_hab ?></td>
                                <td><?= $habitacion->max_per_hab ?></td>
                                <td><?= $habitacion->precio_hab ?></td>
                                <td><?= $habitacion->tipo_hab ?></td>
                                <td><?= $habitacion->descripcion_hab ?></td>
                                <td><?= $habitacion->estado_hab ?></td>
                                <td>
                                    <img width="100" src="<?= base_url() ?>assets/picture/<?= $habitacion->foto_hab ?>">
                                </td>
                                <td class="center-align">
                                    <a href="javascript:void()" onclick="setEditar('<?= $habitacion->id_hab ?>','<?= $habitacion->nro_hab ?>','<?= $habitacion->max_per_hab ?>','<?= $habitacion->precio_hab ?>','<?= $habitacion->tipo_hab ?>','<?= $habitacion->descripcion_hab ?>','<?= $habitacion->estado_hab ?>','<?= $habitacion->foto_hab ?>')" data-toggle="modal" data-target="#myModalEditar"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:void()" onclick="setEliminar('<?= $habitacion->id_hab ?>','<?= $habitacion->foto_hab ?>','<?= $habitacion->nro_hab ?>')" data-toggle="modal" data-target="#myModalEliminar"><i class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col" id="Tipo">
            <h2 class="text-center">Tipo De Hab <a href="javascript:void()" onclick="setGuardarTipo()" data-toggle="modal" data-target="#myModalGuardarTipo"><i class="fas fa-bed"></i></a></h2>
            <div class="table-responsive">
                <table id="tablax1" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <th>ID</th>
                        <th>Tipo</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php foreach ($list_tipos_hab as $tipo) : ?>
                            <tr>
                                <td><?= $tipo->id_tipo_hab ?></td>
                                <td><?= $tipo->tipo_hab ?></td>
                                <td class="center-align">
                                    <a href="javascript:void()" onclick="setEliminarTipo('<?= $tipo->id_tipo_hab ?>')" data-toggle="modal" data-target="#myModalEliminarTipo"><i class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col" id="Estado">
            <h2 class="text-center">Estado De Hab <a href="javascript:void()" onclick="setGuardarEstado()" data-toggle="modal" data-target="#myModalGuardarEstado"><i class="fas fa-bed"></i></a></h2>
            <div class="table-responsive">
                <table id="tablax2" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <th>ID</th>
                        <th>Estado</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php foreach ($list_estados_hab as $estado) : ?>
                            <tr>
                                <td><?= $estado->id_estado_hab ?></td>
                                <td><?= $estado->estado_hab ?></td>
                                <td class="center-align">
                                    <a href="javascript:void()" onclick="setEliminarEstado('<?= $estado->id_estado_hab ?>')" data-toggle="modal" data-target="#myModalEliminarEstado"><i class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalEliminar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Habitacion</h4>
                </div>
                <?= form_open('Habitacion_Controller/eliminar', 'id="formulario_eliminar"') ?>
                <div class="modal-body">
                    <input type="hidden" value="" name="idhab" id="idhab" />
                    <input type="hidden" value="" name="fotoo" id="fotoo" />
                    <input type="hidden" value="" name="nro" id="nro" />
                    Esta seguro de Eliminar la Habitacion?

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalGuardar">
        <div class="modal-dialog" style="width: 1000px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nueva Habitacion</h4>
                </div>
                <?= form_open_multipart('Habitacion_Controller/guardar', 'class="was-validated" id="frm_habitacion_new"') ?>
                <div class="modal-body">

                    <div class="container text-center">
                        <input type="hidden" value="" name="idhabitacion" id="idhabitacion" />
                        <div class="row form-group">
                            <div class="col"><input type="text" id="nro" name="nro" class="form-control" placeholder="Nro" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="number" id="max_per" name="max_per" class="form-control" placeholder="Maximo de personas" /></div>
                            <div class="col"><input type="number" id="precio" name="precio" class="form-control" placeholder="Precio" /></div>
                        </div>
                        <div class="row form-group">

                            <div class="col">
                                <label>
                                    <select class="form-control" id="tipoo" name="tipoo">
                                        <?php foreach ($list_tipos_hab as $tipo) : ?>
                                            <option value="<?php echo $tipo->id_tipo_hab ?>"><?php echo $tipo->tipo_hab ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </label>
                            </div>

                            <div class="col">
                                <label>
                                    <select id="estadoo" name="estadoo" class="form-control">
                                        <?php foreach ($list_estados_hab as $estado) : ?>
                                            <option value="<?php echo $estado->id_estado_hab ?>"><?php echo $estado->estado_hab ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="desc" name="descripcion" class="form-control" placeholder="Descripcion" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-1"><label for="foto">Foto:</label></div>
                            <div class="col-5"><input type="file" id="foto" name="foto" class="form-control-file" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalEditar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Habitacion</h4>
                </div>
                <?= form_open_multipart('Habitacion_Controller/actualizar', 'class="was-validated" id="frm_habitacion_edit"') ?>
                <div class="modal-body">
                    <div class="container text-center">
                        <input type="hidden" value="" name="idhabitacion_" id="idhabitacion_" />
                        <div class="row form-group">
                            <div class="col"><input type="text" id="nro_" name="nro_" class="form-control" placeholder="Nro" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="number" id="max_per_" name="max_per_" class="form-control" placeholder="Maximo de personas" /></div>
                            <div class="col"><input type="number" id="precio_" name="precio_" class="form-control" placeholder="Precio" /></div>
                        </div>
                        <div class="row form-group">

                            <div class="col">
                                <select class="form-control" id="tipooo" name="tipooo">
                                    <?php foreach ($list_tipos_hab as $tipo) : ?>
                                        <option value="<?php echo $tipo->id_tipo_hab ?>"><?php echo $tipo->tipo_hab ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col">
                                <select id="estadooo" name="estadooo" class="form-control">
                                    <?php foreach ($list_estados_hab as $estado) : ?>
                                        <option value="<?php echo $estado->id_estado_hab ?>"><?php echo $estado->estado_hab ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="descripcion_" name="descripcion_" class="form-control" placeholder="Descripcion" /></div>
                        </div>
                        <input type="hidden" value="" name="foto_old" id="foto_old" />
                        <div class="row form-group">
                            <div class="col-1"><label for="foto_">Foto:</label></div>
                            <div class="col-5"><input type="file" id="foto_" name="foto_" class="form-control-file" /></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalEliminarTipo">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Tipo-Habitacion</h4>
                </div>
                <?= form_open('Habitacion_Controller/eliminar_tipo', 'id="formulario_eliminar_tipo"') ?>
                <div class="modal-body">
                    <input type="hidden" value="" name="idtipo" id="idtipo" />
                    Esta seguro de Eliminar el Tipo de Habitacion?

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>
    <div class="modal" id="myModalGuardarTipo">
        <div class="modal-dialog" style="width: 1000px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Tipo de Habitacion</h4>
                </div>
                <?= form_open('Habitacion_Controller/guardar_tipo', 'class="was-validated" id="frm_tipo_hab_new"') ?>
                <div class="modal-body">

                    <div class="container text-center">
                        <input type="hidden" value="" name="idtipo" id="idtipo" />
                        <div class="row form-group">
                            <div class="col"><input type="text" id="tipo_hab" name="tipo_hab" class="form-control" placeholder="Tipo de Habitacion" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>
    <div class="modal" id="myModalEliminarEstado">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Estado-Habitacion</h4>
                </div>
                <?= form_open('Habitacion_Controller/eliminar_estado', 'id="formulario_eliminar_estado"') ?>
                <div class="modal-body">
                    <input type="hidden" value="" name="idestado" id="idestado" />
                    Esta seguro de Eliminar el Estado de Habitacion?

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>
    <div class="modal" id="myModalGuardarEstado">
        <div class="modal-dialog" style="width: 1000px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Estado de Habitacion</h4>
                </div>
                <?= form_open('Habitacion_Controller/guardar_estado', 'class="was-validated" id="frm_estado_hab_new"') ?>
                <div class="modal-body">

                    <div class="container text-center">
                        <input type="hidden" value="" name="idestado" id="idestado" />
                        <div class="row form-group">
                            <div class="col"><input type="text" id="estado_hab" name="estado_hab" class="form-control" placeholder="Estado de Habitacion" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>



</div>
