<script type="text/javascript">
    function setEliminar(id) {
        $("#idres").attr('value', id);
    }

    function setEliminarT(id) {
        $("#idtra_").attr('value', id);
    }

    function setGuardarC(reserva, cliente, descripcion, total) {
        $("#idres_").attr('value', reserva);
        $("#cliente").attr('value', cliente);
        $("#descripcion").attr('value', descripcion);
        $("#total").attr('value', total);
        $("document").ready(function() {
            $('#frm_caja_new').submit(function() {
                $("#submit_guardar").attr('disabled', true);
            });
        });
    }

    function setGuardar() {
        $("document").ready(function() {
            $('#frm_reservacion_new').submit(function() {
                $("#submit_guardar").attr('disabled', true);
            });
        });
    }

    function setEditar(id, id_hab, fechaEntrada, fechaSalida, Detalle, EstadoPagado, id_cli) {
        $("#idreserva_").attr('value', id);
        $("#id_hab_").attr('value', id_hab);
        $("#fecha_entrada_").attr('value', fechaEntrada);
        $("#fecha_salida_").attr('value', fechaSalida);
        $("#detalle_").attr('value', Detalle);
        $("#estado_pagado_").attr('value', EstadoPagado);
        $("#id_cli_").attr('value', id_cli);
        $("document").ready(function() {
            $('#frm_reservacion_edit').submit(function() {
                $("#submit_Actualizar").attr('disabled', true);
            });
        });
    }
</script>
<style>
    th,
    td {
        padding: 0.4rem !important;
    }

    #Reservacion,
    #Transacciones {
        border: 1px solid grey;
        border-radius: 10px;
    }
</style>
<div class="container-fluid">
    <h1 class="text-center">Reservaciones <a href="javascript:void()" onclick="setGuardar()" data-toggle="modal" data-target="#myModalGuardar"><i class="fas fa-calendar"></i></a></h1>
    <div><?= $this->session->flashdata('mensaje'); ?> </div>
    <div class="row" id="Reservacion">
        <div class="col-12">
            <div class="table-responsive">
                <table id="tablax" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <th>ID</th>
                        <th>ID Habitacion</th>
                        <th>Fecha Entrada</th>
                        <th>Fecha Salida</th>
                        <th>Detalle</th>
                        <th>Estado Pagado</th>
                        <th>ID Cliente</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php foreach ($list_reservaciones as $reservacion) : ?>
                            <tr>
                                <td><?= $reservacion->id_reser ?></td>
                                <td><?= $reservacion->id_hab_reser ?></td>
                                <td><?= $reservacion->fecha_entrada_reser ?></td>
                                <td><?= $reservacion->fecha_salida_reser ?></td>
                                <td><?= $reservacion->detalle_reser ?></td>
                                <td><?= $reservacion->estado_pagado_reser ?></td>
                                <td><?= $reservacion->id_cli_reser ?></td>
                                <td class="center-align">
                                    <a href="javascript:void()" onclick="setEditar('<?= $reservacion->id_reser ?>','<?= $reservacion->id_hab_reser ?>','<?= $reservacion->fecha_entrada_reser ?>','<?= $reservacion->fecha_salida_reser ?>','<?= $reservacion->detalle_reser ?>','<?= $reservacion->estado_pagado_reser ?>','<?= $reservacion->id_cli_reser ?>')" data-toggle="modal" data-target="#myModalEditar"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:void()" onclick="setEliminar('<?= $reservacion->id_reser ?>')" data-toggle="modal" data-target="#myModalEliminar"><i class="fas fa-trash-alt"></i></a>
                                    <a href="<?= site_url('Reservacion_Controller/transaccion/' . $reservacion->id_reser) ?>" title="Pagar Reserva">Pagar</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <h1 class="text-center" style="margin-top: 20px;">Transacciones </h1>
    <div class="row" id="Transacciones">
        <div class="col-12">
            <div class="table-responsive">
                <table id="tablax1" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <th>ID</th>
                        <th>ID Reserva</th>
                        <th>ID Cliente</th>
                        <th>Tipo De Pago</th>
                        <th>Descripcion</th>
                        <th>Precio</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php foreach ($list_transacciones as $transaccion) : ?>
                            <tr>
                                <td><?= $transaccion->id_tra ?></td>
                                <td><?= $transaccion->id_reser ?></td>
                                <td><?= $transaccion->id_cli ?></td>
                                <td><?= $transaccion->tipo_de_pago ?></td>
                                <td><?= $transaccion->descripcion ?></td>
                                <td><?= $transaccion->precio ?></td>
                                <td class="center-align">
                                    <a href="javascript:void()" onclick="setEliminarT('<?= $transaccion->id_tra ?>')" data-toggle="modal" data-target="#myModalEliminarT"><i class="fas fa-trash-alt"></i></a>
                                    <a href="javascript:void()" onclick="setGuardarC('<?= $transaccion->id_reser ?>','<?= $transaccion->id_cli ?>','<?= $transaccion->descripcion ?>','<?= $transaccion->precio ?>')" data-toggle="modal" data-target="#myModalGuardarC">Caja</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="modal" id="myModalEliminar">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Cabezera -->
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Reservacion</h4>
                </div>
                <!-- Modal Cuerpo -->
                <?= form_open('Reservacion_Controller/eliminar', 'id="formulario_eliminar""') ?>
                <div class="modal-body">
                    <input type="hidden" value="" name="idres" id="idres" />
                    Esta seguro de Eliminar la Reserva?

                </div>
                <!-- Modal Pie -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalGuardar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nueva Reservacion</h4>
                </div>
                <?= form_open('Reservacion_Controller/guardar', 'class="was-validated" id="frm_reservacion_new"') ?>
                <div class="modal-body">
                    <div class="container text-center">
                        <input type="hidden" value="" name="idreserva" id="idreserva" />
                        <div class="row form-group">

                            <div class="col">
                                <label for="">Habitacion N°: </label>

                                <select id="id_hab" name="id_hab" class="form-control">
                                    <?php foreach ($list_habitaciones as $habitacion) : ?>
                                        <option value="<?php echo $habitacion->id_hab ?>"><?php echo $habitacion->nro_hab ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="date" id="fecha_entrada" name="fecha_entrada" placeholder="Fecha Entrante" class="form-control" /></div>
                            <div class="col"><input type="date" id="fecha_salida" name="fecha_salida" placeholder="Fecha Salida" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="detalle" name="detalle" placeholder="Detalle" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="estado_pagado" name="estado_pagado" placeholder="Estado Pagado (Si/No)" class="form-control" /></div>
                            <div class="col">

                                <select id="id_cli" name="id_cli" class="form-control">
                                    <?php foreach ($list_clientes as $clientes) : ?>
                                        <option value="<?php echo $clientes->id_cli ?>"><?php echo $clientes->nombre_cli ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalEditar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Reservacion</h4>
                </div>
                <?= form_open('Reservacion_Controller/actualizar', 'class="was-validated" id="frm_reservacion_edit"') ?>
                <div class="modal-body">
                    <div class="container text-center">
                        <input type="hidden" value="" name="idreserva_" id="idreserva_" />
                        <div class="row form-group">
                            <div class="col">
                                <label for="">Habitacion N°: </label>

                                <select id="id_hab" name="id_hab" class="form-control">
                                    <?php foreach ($list_habitaciones as $habitacion) : ?>
                                        <option value="<?php echo $habitacion->id_hab ?>"><?php echo $habitacion->nro_hab ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label>Fecha Entrante</label>
                            <div class="col"><input type="date" id="fecha_entrada_" name="fecha_entrada_" placeholder="Fecha Entrante" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <label>Fecha Salida</label>
                            <div class="col"><input type="date" id="fecha_salida_" name="fecha_salida_" placeholder="Fecha Salida" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <label>Detalle</label>
                            <div class="col"><input type="text" id="detalle_" name="detalle_" placeholder="Detalle" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <label>Estado Pagado</label>
                            <div class="col"><input type="text" id="estado_pagado_" name="estado_pagado_" placeholder="Estado Pagado (Si/No)" class="form-control" /></div>
                            <label>Id Cliente</label>
                            <div class="col"><input type="number" id="id_cli_" name="id_cli_" placeholder="ID Cliente" class="form-control" /></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>


    <div class="modal" id="myModalEliminarT">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Transaccion</h4>
                </div>
                <?= form_open('Reservacion_Controller/eliminarT', 'id="formulario_eliminar_') ?>
                <div class="modal-body">
                    <input type="hidden" value="" name="idtra_" id="idtra_" />
                    Esta seguro de Eliminar la Transaccion?
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalGuardarC">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Corte de Caja</h4>
                </div>
                <?= form_open('Caja_Controller/guardar', 'class="was-validated" id="frm_caja_new"') ?>
                <div class="modal-body">
                    <div class="container text-center">
                        <div class="row form-group">
                            <input type="hidden" id="idres_" name="idres_" class="form-control" />
                            <label>Id Cliente</label>
                            <div class="col"><input type="text" id="cliente" name="cliente" placeholder="Cliente" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="descripcion" name="descripcion" placeholder="Descripcion" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <label>Total</label>
                            <div class="col"><input type="number" id="total" name="total" placeholder="Total" class="form-control" /></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>



</div>