<h1 class="text-center">Transaccion</h1>
<div class="row" style="margin-top: 10px;">
    <div class="col-5 text-center">
    <?= form_open('Reservacion_Controller/guardarT', 'class="was-validated" id="frm_transaccion"') ?>
        <div class="row form-group">
            <div class="col"><label for="cant_dias">Cantidad de Dias</label></div>
            <div class="col"><input type="text" id="cant_dias" name="cant_dias" value="<?= $dias ?>" class="form-control" disabled/></div>
        </div>
        <div class="row form-group">
            <div class="col"><label for="id_hab">ID Hab</label></div>
            <div class="col"><input type="text" id="id_hab" name="id_hab" value="<?= $reservacion->id_hab_reser ?>" class="form-control" disabled/></div>
        </div>
        <div class="row form-group">
            <div class="col"><label for="precio_hab">Precio de Hab</label></div>
            <div class="col"><input type="text" id="precio_hab" name="precio_hab" value="<?= $habitacion->precio_hab ?>" class="form-control" disabled/></div>
        </div>
    </div>
    <div class="col text-center">
        <div class="row form-group">
            <div class="col-3"><label for="id_reser">Id Reserva</label></div>
            <div class="col-2"><input type="text" id="id_reser" name="id_reser" value="<?= $reservacion->id_reser ?>"  class="form-control" /></div>
            <div class="col-3"><label for="id_cli">Id Cliente</label></div>
            <div class="col-2"><input type="text" id="id_cli" name="id_cli" value="<?= $reservacion->id_cli_reser ?>" class="form-control" /></div>
        </div>
        <div class="row form-group">
            <div class="col-3"><label for="tipo_de_pago">Tipo de Pago</label></div>
            <div class="col"><?= form_dropdown("tipo_de_pago", array('Efectivo' => 'Efectivo', 'Tarjeta de Credito' => 'Tarjeta de Credito', 'Tarjeta de Debito' => 'Tarjeta de Debito'), array(), array('id' => "tipo_de_pago", 'name'=>"tipo_de_pago", 'class' => 'form-control')) ?></div>
        </div>
        <div class="row form-group">
            <div class="col-3"><label for="descripcion">Descripcion</label></div>
            <div class="col"><input type="text" id="descripcion" name="descripcion" class="form-control" /></div>
        </div>
        <div class="row form-group">
            <div class="col-3"><label for="precio">Precio Total</label></div>
            <div class="col"><input type="number" id="precio" name="precio" value="<?= $totalprecio ?>" class="form-control" /></div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Guardar</button>
            <a class="btn btn-danger" href="<?= site_url('Reservacion_Controller/listado') ?>"><span>Cancelar</span></a>
        </div>
    </div>
    <?= form_close() ?>
</div>
<script type="text/javascript">
    $("document").ready(function () {         
        $('#frm_transaccion').submit(function(){ 
            $("#submit_guardar").attr('disabled', true);
        });       
    });
</script>