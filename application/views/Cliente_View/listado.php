<script type="text/javascript">
    function setEliminar(id, dni_cli) {
        $("#idcli").attr('value', id);
        $("#dnicli").attr('value', dni_cli);
    }

    function setGuardar() {
        $("document").ready(function() {
            $('#frm_huesped_new').submit(function() {
                $("#submit_guardar").attr('disabled', true);
            });
        });
    }

    function setEditar(id,dni,nombre,apellido,domicilio,telefono,email) {
        $("#idcliente_").attr('value', id);
        $("#dni_").attr('value', dni);
        $("#nombre_").attr('value', nombre);
        $("#apellido_").attr('value', apellido);
        $("#domicilio_").attr('value', domicilio);
        $("#telefono_").attr('value', telefono);
        $("#email_").attr('value', email);
        $("document").ready(function() {
            $('#frm_huesped_edit').submit(function() {
                $("#submit_actualizar").attr('disabled', true);
            });
        });
    }
</script>
<style>
    th,
    td {
        padding: 0.4rem !important;
    }

    #Empleado {
        border: 1px solid grey;
        border-radius: 10px;
    }
</style>
<div class="container-fluid">
    <h1 class="text-center">Huspedes <a href="javascript:void()" onclick="setGuardar()" data-toggle="modal" data-target="#myModalGuardar"><i class="fas fa-street-view"></i></a></h1>
    <div><?= $this->session->flashdata('mensaje'); ?> </div>
    <div class="row" id="Huesped">
        <div class="col-12">
            <div class="table-responsive">
                <table id="tablax" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <th>ID</th>
                        <th>DNI</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Domicilio</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php foreach ($list_clientes as $cliente) : ?>
                            <tr>
                                <td><?= $cliente->id_cli ?></td>
                                <td><?= $cliente->dni_cli ?></td>
                                <td><?= $cliente->nombre_cli ?></td>
                                <td><?= $cliente->apellido_cli ?></td>
                                <td><?= $cliente->domicilio_cli ?></td>
                                <td><?= $cliente->telefono_cli ?></td>
                                <td><?= $cliente->email_cli ?></td>
                                <td class="center-align">
                                    <a href="javascript:void()" onclick="setEditar('<?= $cliente->id_cli ?>','<?= $cliente->dni_cli ?>','<?= $cliente->nombre_cli ?>','<?= $cliente->apellido_cli ?>','<?= $cliente->domicilio_cli ?>','<?= $cliente->telefono_cli ?>','<?= $cliente->email_cli ?>')" data-toggle="modal" data-target="#myModalEditar"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:void()" onclick="setEliminar('<?= $cliente->id_cli ?>','<?= $cliente->dni_cli ?>')" data-toggle="modal" data-target="#myModalEliminar"><i class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalEliminar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Huesped</h4>
                </div>
                <?= form_open('Cliente_Controller/eliminar', 'id="formulario_eliminar"') ?>
                <div class="modal-body">
                    <input type="hidden" value="" name="idcli" id="idcli" />
                    <input type="hidden" value="" name="dnicli" id="dnicli" />
                    Esta seguro de Eliminar al Huesped?

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalGuardar">
        <div class="modal-dialog" style="width: 1000px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Huesped</h4>
                </div>
                <?= form_open('Cliente_Controller/guardar', 'class="was-validated" id="frm_huesped_new"') ?>
                <div class="modal-body">

                    <div class="container text-center">
                        <input type="hidden" value="" name="idcliente" id="idcliente" />
                        <div class="row form-group">
                            <div class="col"><input type="number" id="dni" name="dni" class="form-control" placeholder="Dni" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" /></div>
                            <div class="col"><input type="text" id="apellido" name="apellido" class="form-control" placeholder="Apellido" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="domicilio" name="domicilio" class="form-control" placeholder="Domicilio" /></div>
                            <div class="col"><input type="number" id="telefono" name="telefono" class="form-control" placeholder="Telefono" /></div>
                            <div class="col"><input type="text" id="email" name="email" class="form-control" placeholder="Email" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalEditar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Huesped</h4>
                </div>
                <?= form_open('Cliente_Controller/actualizar', 'class="was-validated" id="frm_huesped_edit"') ?>
                <div class="modal-body">
                    <div class="container text-center">
                        <input type="hidden" value="" name="idcliente_" id="idcliente_" />
                        <div class="row form-group">
                            <div class="col"><input type="number" id="dni_" name="dni_" class="form-control" placeholder="Dni" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="nombre_" name="nombre_" class="form-control" placeholder="Nombre" /></div>
                            <div class="col"><input type="text" id="apellido_" name="apellido_" class="form-control" placeholder="Apellido" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col"><input type="text" id="domicilio_" name="domicilio_" class="form-control" placeholder="Domicilio" /></div>
                            <div class="col"><input type="number" id="telefono_" name="telefono_" class="form-control" placeholder="Telefono" /></div>
                            <div class="col"><input type="text" id="email_" name="email_" class="form-control" placeholder="Email" /></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

</div>