<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/fontawesome/css/all.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/DataTables/DataTables-1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/style.css">

    <style>
        .content {
            background: white;
        }

        .content-footer {
            width: (100% - 250px);
            margin-top: 5px;
            padding: 5px;
            margin-left: 250px;
            background: #ccc;
            background-position: center;
            background-size: cover;
            height: 5vh;
            transition: 0.5s;

            z-index: 1;
            position: fixed;
            bottom: 0;
        }

        #check:checked~.content-footer {
            margin-left: 60px;
        }

        @media screen and (max-width: 790px) {

            .content-footer {
                margin-left: 0;
                margin-top: 0;
                padding: 10px 20px;
                transition: 0s;
            }

            #check:checked~.content-footer {
                margin-left: 0;
            }
        }

        footer {
            z-index: 1;
            position: fixed;
            background: #10474b;
            padding: 20px;
            width: calc(100% - 0%);
            bottom: 0;
            height: 30px;
            color: white;
            text-align: center;
        }
    </style>
    <title>Hotel</title>
</head>

<body>

    <input type="checkbox" id="check">

    <header style="padding: 0px; height: 70px;">
        <label for="check">
            <i class="fas fa-bars" id="sidebar_btn"></i>
        </label>
        <div class="left_area" style="margin-left: 10px; width: 230px;">
            <div class="row">
                <div class="col">
                    <a href="<?= site_url('Inicio_Controller') ?>">
                        <h3>Hotel <span>Frontera</span></h3>
                    </a>
                </div>
            </div>
        </div>
        <div style="float: right; margin-right: 10px; margin-top: -30px;">
            <div class="row">
                <div class="col">
                    <a style="color: white;" href="<?= site_url('Login_Controller/logout') ?>">Cerrar Sesión</a>
                </div>
            </div>
        </div>
    </header>

    <div class="mobile_nav">
        <div class="nav_bar">
            <img src="<?= base_url() ?>./assets/picture/<?= $this->session->userdata('foto_emp') ?>" class="mobile_profile_image" alt="">
            <i class="fa fa-bars nav_btn"></i>
        </div>
        <div class="mobile_nav_items">
            <a href="<?= site_url('Reservacion_Controller/listado') ?>"><i class="fas fa-calendar"></i><span>Reservar</span></a>
            <a href="<?= site_url('Caja_Controller/listado') ?>"><i class="fas fa-calculator"></i><span>Caja</span></a>
            <a href="<?= site_url('Habitacion_Controller/listado') ?>"><i class="fas fa-bed"></i><span>Habitaciones</span></a>
            <a href="<?= site_url('Empleado_Controller/listado') ?>"><i class="fas fa-users"></i><span>Empleados</span></a>
            <a href="<?= site_url('Cliente_Controller/listado') ?>"><i class="fas fa-street-view"></i><span>Huespedes</span></a>
            <a href="<?= site_url('Usuario_Controller/listado') ?>"><i class="fas fa-user"></i><span>Usuarios</span></a>
            <a href="<?= site_url('Log_Controller/listado') ?>"><i class="fas fa-user"></i><span>Logs</span></a>
        </div>
    </div>
    <div class="sidebar" style="margin-top: 70px; ">
        <div class="profile_info">
            <img class="profile_image" src="<?= base_url() ?>./assets/picture/<?= $this->session->userdata('foto_emp') ?>">
            <h4><?= $this->session->userdata('nombre_usu') ?></h4>
        </div>
        <a href="<?= site_url('Reservacion_Controller/listado') ?>"><i class="fas fa-calendar"></i><span> Reservar</span></a>
        <a href="<?= site_url('Caja_Controller/listado') ?>"><i class="fas fa-calculator"></i><span> Caja</span></a>
        <a href="<?= site_url('Habitacion_Controller/listado') ?>"><i class="fas fa-bed"></i><span>Habitaciones</span></a>
        <a href="<?= site_url('Empleado_Controller/listado') ?>"><i class="fas fa-users"></i><span>Empleados</span></a>
        <a href="<?= site_url('Cliente_Controller/listado') ?>"><i class="fas fa-street-view"></i><span> Huespedes</span></a>
        <a href="<?= site_url('Usuario_Controller/listado') ?>"><i class="fas fa-user"></i><span> Usuarios</span></a>
        <a href="<?= site_url('Log_Controller/listado') ?>"><i class="fas fa-clone"></i><span> Logs</span></a>
    </div>

    <div class="content">
        <div class="col-12">
            <?= $contenido ?>
        </div>
    </div>

    <div class="content-footer" style="width: 82%; text-align: center;">
    <strong>Copyright &copy; 2021 <a href="#">HF</a></strong>
        <b>TPFinal</b> PSW
    </div>
        
    
    



    <script src="<?= base_url() ?>assets/jquery/jquery-slim.min.js" charset="utf-8"></script>
    <script src="<?= base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?= base_url(); ?>assets/DataTables/datatables.min.js"></script>


    <script>
        $(document).ready(function() {
            $('#tablax').DataTable({
                language: {
                    processing: "Tratamiento en curso...",
                    search: "Buscar&nbsp;:",
                    lengthMenu: "Mostrar _MENU_ registros",
                    info: "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
                    infoEmpty: "No existen datos.",
                    infoFiltered: "(filtrado de _MAX_ elementos en total)",
                    infoPostFix: "",
                    loadingRecords: "Cargando...",
                    zeroRecords: "No se encontraron datos con tu busqueda",
                    emptyTable: "No hay datos disponibles en la tabla.",
                    paginate: {
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultimo"
                    },
                    aria: {
                        sortAscending: ": active para ordenar la columna en orden ascendente",
                        sortDescending: ": active para ordenar la columna en orden descendente"
                    }
                },
                lengthMenu: [
                    [4, 10, 15, -1],
                    [4, 10, 15, "Todo"]
                ],
            });
        });
        $(document).ready(function() {
            $('#tablax1').DataTable({
                language: {
                    processing: "Tratamiento en curso...",
                    search: "Buscar&nbsp;:",
                    lengthMenu: "Mostrar _MENU_ registros",
                    info: "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
                    infoEmpty: "No existen datos.",
                    infoFiltered: "(filtrado de _MAX_ elementos en total)",
                    infoPostFix: "",
                    loadingRecords: "Cargando...",
                    zeroRecords: "No se encontraron datos con tu busqueda",
                    emptyTable: "No hay datos disponibles en la tabla.",
                    paginate: {
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultimo"
                    },
                    aria: {
                        sortAscending: ": active para ordenar la columna en orden ascendente",
                        sortDescending: ": active para ordenar la columna en orden descendente"
                    }
                },
                lengthMenu: [
                    [4, 10, 15, -1],
                    [4, 10, 15, "Todo"]
                ],
            });
        });
        $(document).ready(function() {
            $('#tablax2').DataTable({
                language: {
                    processing: "Tratamiento en curso...",
                    search: "Buscar&nbsp;:",
                    lengthMenu: "Mostrar _MENU_ registros",
                    info: "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
                    infoEmpty: "No existen datos.",
                    infoFiltered: "(filtrado de _MAX_ elementos en total)",
                    infoPostFix: "",
                    loadingRecords: "Cargando...",
                    zeroRecords: "No se encontraron datos con tu busqueda",
                    emptyTable: "No hay datos disponibles en la tabla.",
                    paginate: {
                        first: "Primero",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultimo"
                    },
                    aria: {
                        sortAscending: ": active para ordenar la columna en orden ascendente",
                        sortDescending: ": active para ordenar la columna en orden descendente"
                    }
                },
                lengthMenu: [
                    [4, 10, 15, -1],
                    [4, 10, 15, "Todo"]
                ],
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.nav_btn').click(function() {
                $('.mobile_nav_items').toggleClass('active');
            });
        });
    </script>

</body>

</html>