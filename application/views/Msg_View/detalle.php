<div class="container text-center">
    <h2>Datos de la Consulta</h2>
    <div class="row">
        <div class="col"><Strong><label>Nombre y Apellido:</label></Strong></div>
        <div class="col"><?= $msg->nombre_completo ?></div>
    </div>
    <hr>
    <div class="row">
        <div class="col"><strong><label>Telefono:</label></strong></div>
        <div class="col"><?= $msg->nro_telefono ?></div>
    </div>
    <hr>
    <div class="row">
        <div class="col"><strong><label>Email:</label></strong></div>
        <div class="col"><?= $msg->email ?></div>
    </div>
    <hr>
    <div class="row">
        <div class="col"><strong><label>Fecha Resivida:</label></strong></div>
        <div class="col"><?= $msg->fecha ?></div>
    </div>
    <hr>
    <div class="row">
        <div class="col"><strong><label>Consulta:</label></strong></div>
        <div class="col"><?= $msg->consulta ?></div>
    </div>
    <hr>
</div>