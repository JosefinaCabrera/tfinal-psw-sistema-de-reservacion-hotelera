<script type="text/javascript">
    function setEliminar(id) {
        $("#idusu").attr('value', id);
    }

    function setGuardar() {
        $("document").ready(function() {
            $('#frm_usuario_new').submit(function() {
                $("#submit_guardar").attr('disabled', true);
            });
        });
    }

    function setEditar(id, usuario, idemp) {
        $("#idusuario_").attr('value', id);
        $("#usuario_").attr('value', usuario);
        $("#idemp_").attr('value', idemp);
        $("document").ready(function() {
            $('#frm_usuario_edit').submit(function() {
                $("#submit_guardar").attr('disabled', true);
            });
        });
    }
</script>
<style>
    th,
    td {
        padding: 0.4rem !important;
    }

    #MSG {
        border: 1px solid grey;
        border-radius: 10px;
    }
</style>
<div class="container-fluid">
    <h1 class="text-center">Mensages</h1>
    <div><?= $this->session->flashdata('mensaje'); ?> </div>
    <div class="row" id="MSG">
        <div class="col-12">
            <div class="table-responsive">
                <table id="tablax" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <th>ID</th>
                        <th>Nombre y Apellido</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>fecha</th>
                        <th>leido</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php foreach ($list_msg as $msg) : ?>
                            <tr>
                                <td><?= $msg->id_msg ?></td>
                                <td><?= $msg->nombre_completo ?></td>
                                <td><?= $msg->nro_telefono ?></td>
                                <td><?= $msg->email ?></td>
                                <td><?= $msg->fecha ?></td>
                                <td><?= $msg->leido ?></td>
                                <td class="center-align">
                                <a href="<?= site_url('Msg_Controller/verMsg/' . $msg->id_msg) ?>" title="Leer Msg"><i class="fas fa-search-plus"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>



    <div class="modal" id="myModalEliminar">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Cabezera -->
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Usuario</h4>
                </div>
                <!-- Modal Cuerpo -->
                <?= form_open('Usuario_Controller/eliminar', 'id="formulario_eliminar""') ?>
                <div class="modal-body">
                    <input type="hidden" value="" name="idusu" id="idusu" />
                    Esta seguro de Eliminar al Usuario?

                </div>
                <!-- Modal Pie -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalGuardar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Usuario</h4>
                </div>
                <?= form_open('Usuario_Controller/guardar', 'class="was-validated" id="frm_usuario_new"') ?>
                <div class="modal-body">
                    <div class="container text-center">
                        <input type="hidden" value="" name="idusuario" id="idusuario" />
                        <div class="row form-group">
                            <div class="col-2"><label for="usuario">Usuario</label></div>
                            <div class="col-5"><input type="text" id="usuario" name="usuario" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-2"><label for="password">Password</label></div>
                            <div class="col-5"><input type="text" id="password" name="password" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-2"><label for="idemp">ID_Emp</label></div>
                            <div class="col-5"><input type="number" id="idemp" name="idemp" class="form-control" /></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalEditar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Usuario</h4>
                </div>
                <?= form_open('Usuario_Controller/actualizar', 'class="was-validated" id="frm_usuario_edit"') ?>
                <div class="modal-body">
                    <div class="container text-center">
                        <input type="hidden" value="" name="idusuario_" id="idusuario_" />
                        <div class="row form-group">
                            <div class="col-2"><label for="usuario_">Usuario</label></div>
                            <div class="col-5"><input type="text" id="usuario_" name="usuario_" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-2"><label for="password_">Password</label></div>
                            <div class="col-5"><input type="text" id="password_" name="password_" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-2"><label for="idemp_">ID_Emp</label></div>
                            <div class="col-5"><input type="number" id="idemp_" name="idemp_" class="form-control" /></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>


</div>