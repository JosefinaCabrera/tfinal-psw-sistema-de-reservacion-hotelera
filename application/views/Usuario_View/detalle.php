<h2>Datos del Empleado</h2>
<div class="row">
    <div class="col-1"><label for="">ID:</label></div>
    <div class="col-5"><?= $empleado->id_emp ?></div>
    <div class="col-1"><label for="dni">DNI:</label></div>
    <div class="col-5"><?= $empleado->dni_emp ?></div>
</div>
<div class="row">
    <div class="col-1"><label for="apellido">Apellido:</label></div>
    <div class="col-5"><?= $empleado->apellido_emp ?></div>
    <div class="col-1"><label for="nombre">Nombre:</label></div>
    <div class="col-5"><?= $empleado->nombre_emp ?></div>
</div>
<div class="row">
    <div class="col-1"> <label for="domicilio">Domicilio:</label></div>
    <div class="col-5"><?= $empleado->domicilio_emp ?></div>
</div>
<div class="row">
    <div class="col-1"><label for="telefono">Telefono:</label></div>
    <div class="col-5"><?= $empleado->telefono_emp ?></div>
    <div class="col-1"><label for="email">Email:</label></div>
    <div class="col-5"><?= $empleado->email_emp ?></div>
</div>
<div class="row">
    <div class="col-1"><label for="foto">Foto:</label></div>
    <div class="col-5"><img width="200" style=" border-radius: 100px;" src="<?= base_url() ?>assets/picture/<?= $empleado->foto_emp ?>"></div>
</div>
<div class="row">
    <div class="col-1"><label for="pesto">Puesto:</label></div>
    <div class="col-5"><?= $empleado->puesto_emp ?></div>
</div>