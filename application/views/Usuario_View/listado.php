<script type="text/javascript">
    function setEliminar(id, nom_usu) {
        $("#idusu").attr('value', id);
        $("#nom").attr('value', nom_usu);
    }

    function setGuardar() {
        $("document").ready(function() {
            $('#frm_usuario_new').submit(function() {
                $("#submit_guardar").attr('disabled', true);
            });
        });
    }

    function setEditar(id, usuario, idemp) {
        $("#idusuario_").attr('value', id);
        $("#usuario_").attr('value', usuario);
        $("#idemp_").attr('value', idemp);
        $("document").ready(function() {
            $('#frm_usuario_edit').submit(function() {
                $("#submit_guardar").attr('disabled', true);
            });
        });
    }
</script>
<style>
    th,
    td {
        padding: 0.4rem !important;
    }

    #Usuario {
        border: 1px solid grey;
        border-radius: 10px;
    }
</style>
<div class="container-fluid">
    <h1 class="text-center">Usuarios <a href="javascript:void()" onclick="setGuardar()" data-toggle="modal" data-target="#myModalGuardar"><i class="fas fa-user-plus"></i></a></h1>
    <div><?= $this->session->flashdata('mensaje'); ?> </div>
    <div class="row" id="Usuario">
        <div class="col-12">
            <div class="table-responsive">
                <table id="tablax" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <th>ID</th>
                        <th>Usuario</th>                      
                        <th>ID_Emp</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php foreach ($list_usuarios as $usuario) : ?>
                            <tr>
                                <td><?= $usuario->id_usu ?></td>
                                <td><?= $usuario->nombre_usu ?></td>                               
                                <td><?= $usuario->id_emp ?></td>
                                <td class="center-align">
                                    <a href="javascript:void()" onclick="setEditar('<?= $usuario->id_usu ?>','<?= $usuario->nombre_usu ?>','<?= $usuario->id_emp ?>')" data-toggle="modal" data-target="#myModalEditar"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:void()" onclick="setEliminar('<?= $usuario->id_usu ?>','<?= $usuario->nombre_usu ?>')" data-toggle="modal" data-target="#myModalEliminar"><i class="fas fa-trash-alt"></i></a>
                                    <a href="<?= site_url('Usuario_Controller/verDetalle/' . $usuario->id_emp) ?>" title="Ver Detalle"><i class="fas fa-search-plus"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>



    <div class="modal" id="myModalEliminar">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Cabezera -->
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Usuario</h4>
                </div>
                <!-- Modal Cuerpo -->
                <?= form_open('Usuario_Controller/eliminar', 'id="formulario_eliminar""') ?>
                <div class="modal-body">
                    <input type="hidden" value="" name="idusu" id="idusu" />
                    <input type="hidden" value="" name="nom" id="nom" />
                    Esta seguro de Eliminar al Usuario?

                </div>
                <!-- Modal Pie -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalGuardar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Usuario</h4>
                </div>
                <?= form_open('Usuario_Controller/guardar', 'class="was-validated" id="frm_usuario_new"') ?>
                <div class="modal-body">
                    <div class="container text-center">
                        <input type="hidden" value="" name="idusuario" id="idusuario" />
                        <div class="row form-group">
                            <div class="col-2"><label for="usuario">Usuario</label></div>
                            <div class="col-5"><input type="text" id="usuario" name="usuario" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-2"><label for="password">Password</label></div>
                            <div class="col-5"><input type="text" id="password" name="password" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-2"><label for="idemp">ID_Emp</label></div>
                            <div class="col">
                                <label >
                                <select id="idemp" name="idemp" class="form-control">
                                    <?php foreach ($list_empleados as $empleado) : ?>
                                        <option  value="<?php echo $empleado->id_emp ?>"><?php echo $empleado->nombre_emp ?></option>
                                    <?php endforeach; ?>
                                </select>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>

    <div class="modal" id="myModalEditar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Usuario</h4>
                </div>
                <?= form_open('Usuario_Controller/actualizar', 'class="was-validated" id="frm_usuario_edit"') ?>
                <div class="modal-body">
                    <div class="container text-center">
                        <input type="hidden" value="" name="idusuario_" id="idusuario_" />
                        <div class="row form-group">
                            <div class="col-2"><label for="usuario_">Usuario</label></div>
                            <div class="col-5"><input type="text" id="usuario_" name="usuario_" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-2"><label for="password_">Password</label></div>
                            <div class="col-5"><input type="text" id="password_" name="password_" class="form-control" /></div>
                        </div>
                        <div class="row form-group">
                            <div class="col-2"><label for="idemp_">ID_Emp</label></div>
                            <div class="col">
                                <label >
                                <select id="idemp" name="idemp" class="form-control">
                                    <?php foreach ($list_empleados as $empleado) : ?>
                                        <option  value="<?php echo $empleado->id_emp ?>"><?php echo $empleado->nombre_emp ?></option>
                                    <?php endforeach; ?>
                                </select>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>


</div>