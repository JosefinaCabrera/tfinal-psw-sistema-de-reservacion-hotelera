-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-02-2021 a las 03:06:55
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hotel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cli` int(11) NOT NULL,
  `dni_cli` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nombre_cli` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `apellido_cli` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `domicilio_cli` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `telefono_cli` int(11) NOT NULL,
  `email_cli` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cli`, `dni_cli`, `nombre_cli`, `apellido_cli`, `domicilio_cli`, `telefono_cli`, `email_cli`) VALUES
(1, '23423433', 'Juan', 'Cruz', 'Av Belgrano N°123', 23423, 'ju@gmail.com'),
(2, '43534534', 'Carlos', 'Mendoza', 'Av Madrid N°3223', 453543, 'car@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `corte_caja`
--

CREATE TABLE `corte_caja` (
  `id_caja` int(11) NOT NULL,
  `cliente` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `fecha` date NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `corte_caja`
--

INSERT INTO `corte_caja` (`id_caja`, `cliente`, `descripcion`, `fecha`, `total`) VALUES
(3, 'Juan Cruz', '', '2021-02-04', 5000),
(8, 'Carlos Mendoza', 'pagado', '2021-02-07', 7500),
(9, 'Juan Cruz', '', '2021-02-08', 4000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `id_emp` int(11) NOT NULL,
  `dni_emp` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nombre_emp` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `apellido_emp` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `domicilio_emp` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `telefono_emp` int(11) NOT NULL,
  `email_emp` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `foto_emp` varchar(100) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `puesto_emp` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `horario_emp` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `sueldo_emp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id_emp`, `dni_emp`, `nombre_emp`, `apellido_emp`, `domicilio_emp`, `telefono_emp`, `email_emp`, `foto_emp`, `puesto_emp`, `horario_emp`, `sueldo_emp`) VALUES
(1, '432423423', 'Jose', 'Martinez', 'Av. Simepre Viva N°234', 234234234, 'jose@gmail.com', 'marck.png', 'Administrador', '8:00 a 13:00 hs', 30000),
(2, '40762347', 'Josefina', 'Cabrera', 'Av Belgrano N°123', 12312332, 'jo@gmail.com', 'frida1.jpg', 'Administrador', '13 a 19 hs', 30000),
(3, '34987678', 'Brad', 'Pitt', 'Av Madrid N°3223', 2147483647, 'brad@gmail.com', 'brad-pitt-getty1-a.jpg', 'Resepcion', '8 a 14 hs', 20000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_habitacion`
--

CREATE TABLE `estado_habitacion` (
  `id_estado_hab` int(11) NOT NULL,
  `estado_hab` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `estado_habitacion`
--

INSERT INTO `estado_habitacion` (`id_estado_hab`, `estado_hab`) VALUES
(1, 'Disponible'),
(2, 'No Disponible');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

CREATE TABLE `habitacion` (
  `id_hab` int(11) NOT NULL,
  `nro_hab` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `max_per_hab` int(11) NOT NULL,
  `precio_hab` int(11) NOT NULL,
  `tipo_hab` int(11) NOT NULL,
  `descripcion_hab` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `estado_hab` int(11) NOT NULL,
  `foto_hab` varchar(100) COLLATE utf8mb4_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `habitacion`
--

INSERT INTO `habitacion` (`id_hab`, `nro_hab`, `max_per_hab`, `precio_hab`, `tipo_hab`, `descripcion_hab`, `estado_hab`, `foto_hab`) VALUES
(1, '1', 2, 2000, 1, '1 cama, 1 baño, 1 tv ', 1, 'hab1.jpg'),
(2, '2', 2, 2500, 2, '1 cama doble, 1 baño, 1 tv', 1, 'hab2.jpg'),
(6, '3', 2, 12000, 1, '1 cama doble, 1 baño, 1 tv', 1, 'hab6.jpeg'),
(8, '4', 3, 2400, 2, '1 cama doble, 1 baño, 1 tv', 1, 'hab3.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE `logs` (
  `id_log` int(11) NOT NULL,
  `log_detalle` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `logs`
--

INSERT INTO `logs` (`id_log`, `log_detalle`, `fecha`, `hora`) VALUES
(1, '\'el usuario\'.ned.\'Agrego la habitacion N°\'.3', '2021-02-07', '07:28:46'),
(2, 'El usuariohomeroAgrego la habitacion N°4', '2021-02-07', '08:05:14'),
(3, 'El usuario homero Elimino la habitacion N° 0', '2021-02-07', '08:19:51'),
(4, 'El usuario homero Elimino la habitacion N° 3', '2021-02-07', '08:20:19'),
(5, 'El usuario homero Agrego un Empleado con dni: 40987678', '2021-02-07', '08:48:44'),
(6, 'El usuario homero Elimino al Empleado con dni: 40987678', '2021-02-07', '08:50:54'),
(7, 'El usuario homero Agrego un Empleado con dni: 40987678', '2021-02-07', '09:00:40'),
(8, 'El usuario homero Agrego al Ususario con id: josi', '2021-02-07', '09:00:58'),
(9, 'El usuario homero Agrego un Empleado con dni: ', '2021-02-07', '09:16:38'),
(10, 'El usuario homero Agrego la habitacion N° 3', '2021-02-07', '10:34:47'),
(11, 'El usuario homero Actualizo la habitacion N° 3', '2021-02-07', '10:49:17'),
(12, 'El usuario homero Agrego la reservacion con id: 1', '2021-02-07', '11:19:46'),
(13, 'El usuario homero Actualizo la habitacion N° 3', '2021-02-07', '11:28:18'),
(14, 'El usuario homero Agrego la habitacion N° 4', '2021-02-07', '11:30:04'),
(15, 'El usuario homero Elimino la habitacion N° 4', '2021-02-07', '11:30:11'),
(16, 'El usuario homero Actualizo al Elmpleado con dni: 432423423', '2021-02-08', '02:06:49'),
(17, 'El usuario homero Elimino al Usuario: 0', '2021-02-08', '02:09:51'),
(18, 'El usuario homero Elimino al Empleado con dni: 2342342', '2021-02-08', '02:10:07'),
(19, 'El usuario homero Elimino al Empleado con dni: 40987678', '2021-02-08', '02:11:37'),
(20, 'El usuario homero Agrego un Empleado con dni: 40762347', '2021-02-08', '02:12:35'),
(21, 'El usuario homero Agrego un Empleado con dni: 34987678', '2021-02-08', '02:14:20'),
(22, 'El usuario homero Actualizo el Usuario con dni: Jose', '2021-02-08', '02:15:12'),
(23, 'El usuario Jose Agrego al Ususario con id: Josefina', '2021-02-08', '02:24:40'),
(24, 'El usuario Jose Agrego al Ususario con id: Brad', '2021-02-08', '02:25:04'),
(25, 'El usuario Jose cerro Caja del Cliente Juan Cruz', '2021-02-08', '02:30:24'),
(26, 'El usuario Jose Agrego la reservacion con id: 2', '2021-02-08', '02:32:47'),
(27, 'El usuario Jose Agrego la transaccion de la reserva con id: 9', '2021-02-08', '02:49:35'),
(28, 'El usuario Jose cerro Caja del Cliente Carlos Mendoza', '2021-02-08', '02:50:22'),
(29, 'El usuario Jose Agrego la reservacion con id: 1', '2021-02-08', '03:01:51'),
(30, 'El usuario Jose Agrego la transaccion de la reserva con id: 10', '2021-02-08', '03:02:01'),
(31, 'El usuario Jose cerro Caja del Cliente Juan Cruz', '2021-02-08', '03:02:10'),
(32, 'El usuario Josefina Agrego la habitacion N° 4', '2021-02-08', '03:04:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensages`
--

CREATE TABLE `mensages` (
  `id_msg` int(11) NOT NULL,
  `nombre_completo` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nro_telefono` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `consulta` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `fecha` date NOT NULL,
  `leido` varchar(2) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `mensages`
--

INSERT INTO `mensages` (`id_msg`, `nombre_completo`, `nro_telefono`, `email`, `consulta`, `fecha`, `leido`) VALUES
(1, 'Gustavo Arias', '234234', 'gus@gmail.com', 'acepta mascotas en el hotel', '2021-02-07', 'Si'),
(2, 'Juan Garcia', '432423', 'jua@gmail.com', 'aceptan tarjetas de credito', '2021-02-07', 'Si'),
(3, 'Josefina Cabrera', '234234', 'jo@gmail.com', '------', '2021-02-07', 'Si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservacion`
--

CREATE TABLE `reservacion` (
  `id_reser` int(11) NOT NULL,
  `id_hab_reser` int(11) NOT NULL,
  `fecha_entrada_reser` date NOT NULL,
  `fecha_salida_reser` date NOT NULL,
  `detalle_reser` text COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `estado_pagado_reser` varchar(2) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `id_cli_reser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `reservacion`
--

INSERT INTO `reservacion` (`id_reser`, `id_hab_reser`, `fecha_entrada_reser`, `fecha_salida_reser`, `detalle_reser`, `estado_pagado_reser`, `id_cli_reser`) VALUES
(6, 2, '2021-02-03', '2021-02-05', '', 'si', 1),
(9, 2, '2021-02-07', '2021-02-10', 'va con un mascota', 'si', 2),
(10, 1, '2021-02-07', '2021-02-09', '', 'si', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_habitacion`
--

CREATE TABLE `tipo_habitacion` (
  `id_tipo_hab` int(11) NOT NULL,
  `tipo_hab` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `tipo_habitacion`
--

INSERT INTO `tipo_habitacion` (`id_tipo_hab`, `tipo_hab`) VALUES
(1, 'Sencilla'),
(2, 'Doble'),
(5, 'Matrimonial');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaccion`
--

CREATE TABLE `transaccion` (
  `id_tra` int(11) NOT NULL,
  `id_reser` int(11) NOT NULL,
  `id_cli` int(11) NOT NULL,
  `tipo_de_pago` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `transaccion`
--

INSERT INTO `transaccion` (`id_tra`, `id_reser`, `id_cli`, `tipo_de_pago`, `descripcion`, `precio`) VALUES
(6, 6, 1, 'Efectivo', '', 5000),
(8, 9, 2, 'Efectivo', '', 7500),
(9, 10, 1, 'Efectivo', '', 4000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usu` int(11) NOT NULL,
  `nombre_usu` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `password_usu` varchar(100) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `id_emp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usu`, `nombre_usu`, `password_usu`, `id_emp`) VALUES
(1, 'Jose', '90e528618534d005b1a7e7b7a367813f', 1),
(13, 'Josefina', 'd373a14e8df9ba9387b799f2fb356b1b', 2),
(14, 'Brad', '389c28fac00187c7a4821098a6346d38', 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cli`),
  ADD UNIQUE KEY `dni_cli` (`dni_cli`),
  ADD UNIQUE KEY `email_cli` (`email_cli`);

--
-- Indices de la tabla `corte_caja`
--
ALTER TABLE `corte_caja`
  ADD PRIMARY KEY (`id_caja`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id_emp`),
  ADD UNIQUE KEY `dni_emp` (`dni_emp`),
  ADD UNIQUE KEY `email_emp` (`email_emp`),
  ADD UNIQUE KEY `dni_emp_2` (`dni_emp`);

--
-- Indices de la tabla `estado_habitacion`
--
ALTER TABLE `estado_habitacion`
  ADD PRIMARY KEY (`id_estado_hab`);

--
-- Indices de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD PRIMARY KEY (`id_hab`),
  ADD UNIQUE KEY `nro_hab` (`nro_hab`),
  ADD KEY `tipo_hab` (`tipo_hab`),
  ADD KEY `estado_hab` (`estado_hab`);

--
-- Indices de la tabla `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id_log`);

--
-- Indices de la tabla `mensages`
--
ALTER TABLE `mensages`
  ADD PRIMARY KEY (`id_msg`);

--
-- Indices de la tabla `reservacion`
--
ALTER TABLE `reservacion`
  ADD PRIMARY KEY (`id_reser`),
  ADD KEY `id_hab_reser` (`id_hab_reser`),
  ADD KEY `id_cli_reser` (`id_cli_reser`);

--
-- Indices de la tabla `tipo_habitacion`
--
ALTER TABLE `tipo_habitacion`
  ADD PRIMARY KEY (`id_tipo_hab`);

--
-- Indices de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD PRIMARY KEY (`id_tra`),
  ADD KEY `id_reser` (`id_reser`),
  ADD KEY `id_cli` (`id_cli`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usu`),
  ADD UNIQUE KEY `id_emp` (`id_emp`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `corte_caja`
--
ALTER TABLE `corte_caja`
  MODIFY `id_caja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id_emp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `estado_habitacion`
--
ALTER TABLE `estado_habitacion`
  MODIFY `id_estado_hab` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  MODIFY `id_hab` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `logs`
--
ALTER TABLE `logs`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `mensages`
--
ALTER TABLE `mensages`
  MODIFY `id_msg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `reservacion`
--
ALTER TABLE `reservacion`
  MODIFY `id_reser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tipo_habitacion`
--
ALTER TABLE `tipo_habitacion`
  MODIFY `id_tipo_hab` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  MODIFY `id_tra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD CONSTRAINT `habitacion_ibfk_1` FOREIGN KEY (`tipo_hab`) REFERENCES `tipo_habitacion` (`id_tipo_hab`),
  ADD CONSTRAINT `habitacion_ibfk_2` FOREIGN KEY (`estado_hab`) REFERENCES `estado_habitacion` (`id_estado_hab`);

--
-- Filtros para la tabla `reservacion`
--
ALTER TABLE `reservacion`
  ADD CONSTRAINT `reservacion_ibfk_1` FOREIGN KEY (`id_hab_reser`) REFERENCES `habitacion` (`id_hab`),
  ADD CONSTRAINT `reservacion_ibfk_2` FOREIGN KEY (`id_cli_reser`) REFERENCES `cliente` (`id_cli`);

--
-- Filtros para la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD CONSTRAINT `transaccion_ibfk_1` FOREIGN KEY (`id_reser`) REFERENCES `reservacion` (`id_reser`),
  ADD CONSTRAINT `transaccion_ibfk_2` FOREIGN KEY (`id_cli`) REFERENCES `cliente` (`id_cli`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_emp`) REFERENCES `empleado` (`id_emp`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
